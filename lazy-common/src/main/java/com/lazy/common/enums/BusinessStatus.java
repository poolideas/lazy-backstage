package com.lazy.common.enums;

/**
 * 操作状态
 * 
 * @author yang
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
