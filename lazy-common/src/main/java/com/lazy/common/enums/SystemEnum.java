package com.lazy.common.enums;

public enum SystemEnum {
    SUCCESS(100000, "SUCCESS", "成功"),
    FAIL(100001, "FAIL", "失败"),
    REQUEST_PARAMS_VALID_FAILED(100003, "request params valid failed", "请求参数校验失败"),
    RUNTIME_ERROR(100005, "runtime error", "运行时异常"),
    HTTP_FAIL(100006, "internal HTTP request exception", "内部http请求异常"),
    BUSSINESS_FAIL(100005, "bussiness run error", "业务运行时异常"),
    ORGAN_VALID(100007, "organ null exception", "用户信息异常，无法获取机构"),
    NO_PERMISSION(100008, "no permission", "暂无权限，请联系管理员"),
    USER_NOT_EXIST(100009, "user not exist", "用户不存在"),
    DEPOSIT_NOT_ENOUGH(100010, "deposit", "押金不足"),
    ORGAN_NAME_REPEAT(100011, "organ name repeat", "机构名称重复"),
    VALIDATECODE_NOT_NULL(100012, "validateCode not null", "验证码不能为空"),
    VALIDATECODE_INCORRECT(100013, "validateCode incorrect", "验证码不正确"),
    CAN_NOT_UPLOAD(100014, "can not upload", "该模块统一做不覆盖导入处理"),
    ID_NOT_NULL(100015, "id not null", "不需要传id"),
    ID_NULL(100016, "id null", "请添加id值"),

    ;

    private int code;
    private String msg;
    private String memo;

    SystemEnum(int code, String msg, String memo) {
        this.code = code;
        this.msg = msg;
        this.memo = memo;
    }

    public int code() {
        return this.code;
    }

    public String msg() {
        return this.msg;
    }

    public String memo() {
        return this.memo;
    }

    public static SystemEnum apiByCode(int code) {
        SystemEnum[] var1 = values();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            SystemEnum api = var1[var3];
            if (api.code() == code) {
                return api;
            }
        }

        return FAIL;
    }
}
