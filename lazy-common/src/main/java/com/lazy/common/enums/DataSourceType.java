package com.lazy.common.enums;

/**
 * 数据源
 * 
 * @author yang
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
