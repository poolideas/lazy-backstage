
package com.lazy.common.core.domain;

import com.lazy.common.enums.SystemEnum;

import java.io.Serializable;

/**
 * 访问结果封装
 *
 * @Author yangpeng
 * @CreateDate: 2019/6/13
 * @Version: 1.0
 */
public class R<T> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6095433538316185017L;

    private boolean success;

    private int code;
    private String message;
    private T data;

    public R() {
    }


    public R(T data) {
        this.code = SystemEnum.SUCCESS.code();
        this.message = SystemEnum.SUCCESS.msg();
        this.data = data;
        this.success = true;
    }


    public R(SystemEnum systemEnum) {

        this.code = systemEnum.code();
        this.setMessage(systemEnum.memo());
        this.success = systemEnum.code() == SystemEnum.SUCCESS.code();
    }

    public R(SystemEnum systemEnum, T data) {

        this.code = systemEnum.code();
        this.setMessage(systemEnum.memo());
        this.data = data;
        this.success = systemEnum.code() == SystemEnum.SUCCESS.code();
    }

    public R(SystemEnum systemEnum, String message) {

        this.code = systemEnum.code();
        this.setMessage(message);
        this.success = systemEnum.code() == systemEnum.SUCCESS.code();
    }

    public R(SystemEnum systemEnum, String message, T data) {

        this.code = systemEnum.code();
        this.setMessage(message);
        this.setData(data);
        this.success = systemEnum.code() == systemEnum.SUCCESS.code();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static R ok() {
        R r = new R();
        r.code = SystemEnum.SUCCESS.code();
        r.message = SystemEnum.SUCCESS.msg();
        r.success = true;
        return r;
    }

    public static R ok(Object data) {
        R r = new R();
        r.code = SystemEnum.SUCCESS.code();
        r.message = SystemEnum.SUCCESS.msg();
        r.success = true;
        r.data = data;
        return r;
    }

}
