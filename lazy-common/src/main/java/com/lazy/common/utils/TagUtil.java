package com.lazy.common.utils;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TagUtil {


    public static void main(String[] args) {
        long start = System.nanoTime();
        String sss = "032506080907020325060809070203250608090702032506080907020325060809070203250608090702032506080907020325060809070203250608090702032506080907020325060809070203250608090702" ;
        List<Integer> tagList = getTagList(sss, 10, false);
        System.out.println(System.nanoTime() - start);
        System.out.println(JSON.toJSONString(tagList));
    }

    /**
     * 获取标签列表
     *
     * @return
     */
    public static String getTagStrByList(List<String> tags) {
        StringBuilder sb = new StringBuilder();
        for (String tag : tags) {
            if (tag.length() == 1) {
                sb.append("0");
            }
            sb.append(tag);
        }
        return sb.toString();
    }

    /**
     * 获取标签列表
     *
     * @param tagStr 标签字符串
     * @param length 需要长度 0代表全部
     * @param sort   true 正序 false 倒序
     * @return
     */
    public static List<Integer> getTagList(String tagStr, int length, Boolean sort) {

        List<Integer> result = new ArrayList<>();
        List<Integer> strList = getStrList(tagStr, 2);
        if (length == 0 && sort) {
            return strList;
        }
        if (length == 0) {
            length = strList.size();
        }
        if (length > strList.size()) {
            length = strList.size();
        }
        if (!sort) {
            for (int i = strList.size() - 1; i >= strList.size() - length; i--) {
                result.add(strList.get(i));
            }
        } else {
            for (int i = 0; i < length; i++) {
                result.add(strList.get(i));
            }

        }
        return result;
    }


    /**
     * 把原始字符串分割成指定长度的字符串列表
     *
     * @param inputString 原始字符串
     * @param length      指定长度
     * @return
     */
    public static List<Integer> getStrList(String inputString, int length) {
        int size = inputString.length() / length;
        if (inputString.length() % length != 0) {
            size += 1;
        }
        return getIntList(inputString, length, size);
    }

    public static List<Integer> getIntList(String inputString, int length) {
        int size = inputString.length() / length;
        if (inputString.length() % length != 0) {
            size += 1;
        }
        return getIntList(inputString, length, size);
    }

    /**
     * 把原始字符串分割成指定长度的字符串列表
     *
     * @param inputString 原始字符串
     * @param length      指定长度
     * @param size        指定列表大小
     * @return
     */
    public static List<Integer> getIntList(String inputString, int length, int size) {
        List<Integer> list = new ArrayList<>();
        for (int index = 0; index < size; index++) {
            String childStr = substring(inputString, index * length,
                    (index + 1) * length);
            list.add(Integer.parseInt(childStr));
        }
        return list;
    }

    /**
     * 分割字符串，如果开始位置大于字符串长度，返回空
     *
     * @param str 原始字符串
     * @param f   开始位置
     * @param t   结束位置
     * @return
     */
    public static String substring(String str, int f, int t) {
        if (f > str.length())
            return null;
        if (t > str.length()) {
            return str.substring(f, str.length());
        } else {
            return str.substring(f, t);
        }
    }

    /**
     * 才干计数器 map k=code v=数量
     *
     * @param tags
     * @return
     */
    public static Map<Integer, Integer> getTagCount(List<String> tags) {
        Map<Integer, Integer> map = new HashMap<>();
        tags.forEach(t -> {
            List<Integer> tagList = TagUtil.getTagList(t, 34, true);
            tagList.forEach(code -> {
                if (map.containsKey(code)) {
                    map.put(code, map.get(code) + 1);
                } else {
                    map.put(code, 1);
                }
            });
        });
        return map;
    }
}
