package com.lazy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lazy.system.domain.entity.OpenClass;

/**
 * 公开课Service接口
 *
 * @author yang
 * @date 2021-01-21
 */
public interface IOpenClassService extends IService<OpenClass> {

}
