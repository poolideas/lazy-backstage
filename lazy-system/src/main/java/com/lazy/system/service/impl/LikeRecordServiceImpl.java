package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.LikeRecord;
import com.lazy.system.mapper.LikeRecordMapper;
import com.lazy.system.service.ILikeRecordService;
import org.springframework.stereotype.Service;

/**
 * 点赞Service业务层处理
 *
 * @author yang
 * @date 2021-02-09
 */
@Service
public class LikeRecordServiceImpl extends ServiceImpl<LikeRecordMapper, LikeRecord> implements ILikeRecordService {

    @Override
    public int likeCount(Long id, Integer code) {
        LambdaQueryWrapper<LikeRecord> likeQueryWrapper = Wrappers.lambdaQuery();
        likeQueryWrapper.eq(LikeRecord::getTopicId, id);
        likeQueryWrapper.eq(LikeRecord::getTopicType, code);
        return this.count(likeQueryWrapper);
    }
}
