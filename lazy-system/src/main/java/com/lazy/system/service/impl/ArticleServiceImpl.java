package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.Article;
import com.lazy.system.mapper.ArticleMapper;
import com.lazy.system.service.IArticleService;
import org.springframework.stereotype.Service;

/**
 * 文章Service业务层处理
 *
 * @author yang
 * @date 2021-01-21
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements IArticleService {

}
