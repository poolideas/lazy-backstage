package com.lazy.system.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.AwakeDiary;
import com.lazy.system.mapper.AwakeDiaryMapper;
import com.lazy.system.service.IAwakeDiaryService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 觉醒日记Service业务层处理
 *
 * @author yang
 * @date 2021-01-23
 */
@Service
public class AwakeDiaryServiceImpl extends ServiceImpl<AwakeDiaryMapper, AwakeDiary> implements IAwakeDiaryService {

}
