package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.dto.AccountImgAndNickNameDto;
import com.lazy.system.domain.entity.Account;
import com.lazy.system.domain.entity.AccountPlatform;
import com.lazy.system.domain.entity.UserAwaker;
import com.lazy.system.domain.entity.UserTeacher;
import com.lazy.system.domain.enums.UserTypeEnum;
import com.lazy.system.domain.query.applet.AppletUserRegisterAndLoginParam;
import com.lazy.system.mapper.AccountMapper;
import com.lazy.system.service.IAccountPlatformService;
import com.lazy.system.service.IAccountService;
import com.lazy.system.service.IUserAwakerService;
import com.lazy.system.service.IUserTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 账户Service业务层处理
 *
 * @author yang
 * @date 2021-01-02
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

    @Autowired
    private IAccountPlatformService accountPlatformService;
    @Autowired
    private IUserAwakerService userAwakerService;
    @Autowired
    private IUserTeacherService userTeacherService;

    @Override
    @Transactional
    public Long registerOrLogin(AppletUserRegisterAndLoginParam dto) {
        String phone = dto.getPhone();
        String openid = dto.getOpenid();
        String avatarUrl = dto.getAvatarUrl();
        String nickName = dto.getNickName();
        Integer type = dto.getType();
        Account account = getAccountOrRegister(phone, openid, avatarUrl, nickName, type);
        return account.getId();
    }

    private Account getAccountOrRegister(String phone, String openid, String avatarUrl, String nickName, Integer type) {
        Account account = this.getOne(Wrappers.<Account>lambdaQuery().eq(Account::getPhone, phone));
        if (account == null) {
            account = new Account();
            account.setPhone(phone);
            account.setType(type);
            this.save(account);
            AccountPlatform accountPlatform = accountPlatformService.getOne(Wrappers.<AccountPlatform>lambdaQuery()
                    .eq(AccountPlatform::getPlatformToken, openid));
            if (accountPlatform == null) {
                AccountPlatform platformEntity = new AccountPlatform();
                platformEntity.setUid(account.getId()).setAvatar(avatarUrl).setType(1).setNickname(nickName).setPlatformToken(openid);
                accountPlatformService.save(platformEntity);
            } else {
                accountPlatform.setUid(account.getId()).setAvatar(avatarUrl).setNickname(nickName);
                accountPlatformService.updateById(accountPlatform);
            }
            if (UserTypeEnum.AWAKE.getCode().equals(type)) {
                UserAwaker userAwaker = new UserAwaker();
                userAwaker.setUid(account.getId()).setAvatar(avatarUrl).setNickname(nickName);
                userAwakerService.save(userAwaker);
            } else if (UserTypeEnum.TEACHER.getCode().equals(type)) {
                UserTeacher userTeacher = new UserTeacher();
                userTeacher.setUid(account.getId()).setAvatar(avatarUrl).setNickname(nickName);
                userTeacherService.save(userTeacher);
            }

        }
        return account;
    }

    @Override
    public Account register(String phone, String openid, Integer type) {
        return getAccountOrRegister(phone, openid, null, null, type);
    }

    @Override
    public String getNameByUid(Long uid) {
        Account account = this.getById(uid);
        if (UserTypeEnum.AWAKE.getCode().equals(account.getType())) {
            UserAwaker awaker = userAwakerService.getUserByUid(account.getId());
            return awaker.getNickname();
        } else if (UserTypeEnum.TEACHER.getCode().equals(account.getType())) {
            UserTeacher teacher = userTeacherService.getUserByUid(account.getId());
            return teacher.getName();
        }
        return null;
    }

    @Override
    public AccountImgAndNickNameDto getImgAndNameByUid(Long uid) {

        AccountImgAndNickNameDto dto = new AccountImgAndNickNameDto();
        Account account = this.getById(uid);
        dto.setPhone(account.getPhone());
        if (account.getType().equals(UserTypeEnum.TEACHER.getCode())) {
            UserTeacher teacher = userTeacherService.getUserByUid(account.getId());
            if (teacher != null) {
                dto.setName(teacher.getName());
                dto.setAvatar(teacher.getAvatar());
            }
        } else if (account.getType().equals(UserTypeEnum.AWAKE.getCode())) {
            UserAwaker awaker = userAwakerService.getUserByUid(account.getId());
            if (awaker != null) {
                dto.setName(awaker.getName());
                dto.setAvatar(awaker.getAvatar());
            }
        }
        return dto;
    }

}
