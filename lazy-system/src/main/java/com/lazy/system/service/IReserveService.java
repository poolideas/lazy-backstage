package com.lazy.system.service;

import com.lazy.system.domain.entity.Reserve;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 预约Service接口
 *
 * @author yang
 * @date 2021-02-09
 */
public interface IReserveService extends IService<Reserve> {

}
