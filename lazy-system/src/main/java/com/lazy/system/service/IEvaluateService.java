package com.lazy.system.service;

import com.lazy.system.domain.entity.Evaluate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 预约评价Service接口
 *
 * @author yang
 * @date 2021-02-09
 */
public interface IEvaluateService extends IService<Evaluate> {

}
