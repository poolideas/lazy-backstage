package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.system.domain.enums.TopicTypeEnum;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.BrowseRecord;
import com.lazy.system.mapper.BrowseRecordMapper;
import com.lazy.system.service.IBrowseRecordService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 浏览Service业务层处理
 *
 * @author yang
 * @date 2021-02-09
 */
@Service
public class BrowseRecordServiceImpl extends ServiceImpl<BrowseRecordMapper, BrowseRecord> implements IBrowseRecordService {

    @Override
    public int browseCount(Long id, Integer code) {
        LambdaQueryWrapper<BrowseRecord> browseWrapper = Wrappers.lambdaQuery();
        browseWrapper.eq(BrowseRecord::getTopicId, id);
        browseWrapper.eq(BrowseRecord::getTopicType, code);
        return this.count(browseWrapper);
    }
}
