package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.CommentReply;
import com.lazy.system.mapper.CommentReplyMapper;
import com.lazy.system.service.ICommentReplyService;
import org.springframework.stereotype.Service;

/**
 * 评论回复Service业务层处理
 *
 * @author yang
 * @date 2021-01-21
 */
@Service
public class CommentReplyServiceImpl extends ServiceImpl<CommentReplyMapper, CommentReply> implements ICommentReplyService {

    @Override
    public int commentReplyCount(Long id, Integer code) {
        LambdaQueryWrapper<CommentReply> commentReplyWrapper = Wrappers.lambdaQuery();
        commentReplyWrapper.eq(CommentReply::getTopicId, id);
        commentReplyWrapper.eq(CommentReply::getTopicType, code);
        return this.count(commentReplyWrapper);
    }
}
