package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.Comment;
import com.lazy.system.domain.entity.CommentReply;
import com.lazy.system.mapper.CommentMapper;
import com.lazy.system.service.ICommentReplyService;
import com.lazy.system.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 评论Service业务层处理
 *
 * @author yang
 * @date 2021-01-21
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements ICommentService {

    @Autowired
    private ICommentReplyService commentReplyService;

    @Override
    public int commentCount(Long id, Integer code) {

        LambdaQueryWrapper<Comment> commentWrapper = Wrappers.lambdaQuery();
        commentWrapper.eq(Comment::getTopicId, id);
        commentWrapper.eq(Comment::getTopicType, code);
        return this.count(commentWrapper);
    }

    @Override
    @Transactional
    public Boolean deleteByIds(List<String> list) {
        boolean result1 = this.removeByIds(list);
        LambdaQueryWrapper<CommentReply> w = Wrappers.<CommentReply>lambdaQuery()
                .in(CommentReply::getCommentId, list);
        boolean result2 = commentReplyService.remove(w);
        return result1 && result2;
    }
}
