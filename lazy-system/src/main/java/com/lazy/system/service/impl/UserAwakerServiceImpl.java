package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.UserAwaker;
import com.lazy.system.mapper.UserAwakerMapper;
import com.lazy.system.service.IUserAwakerService;
import org.springframework.stereotype.Service;

/**
 * 觉醒者用户信息Service业务层处理
 *
 * @author yang
 * @date 2021-01-02
 */
@Service
public class UserAwakerServiceImpl extends ServiceImpl<UserAwakerMapper, UserAwaker> implements IUserAwakerService {

    @Override
    public UserAwaker getUserByUid(Long uid) {
        return this.getOne(Wrappers.<UserAwaker>lambdaQuery().eq(UserAwaker::getUid, uid));
    }
}
