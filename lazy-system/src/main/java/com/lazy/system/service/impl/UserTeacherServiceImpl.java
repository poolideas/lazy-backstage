package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.convert.UserTeacherConvert;
import com.lazy.system.domain.dto.UserTeacherAddDto;
import com.lazy.system.domain.entity.Account;
import com.lazy.system.domain.entity.UserTeacher;
import com.lazy.system.mapper.UserTeacherMapper;
import com.lazy.system.service.IAccountService;
import com.lazy.system.service.IUserTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 教练Service业务层处理
 *
 * @author yang
 * @date 2021-01-23
 */
@Service
public class UserTeacherServiceImpl extends ServiceImpl<UserTeacherMapper, UserTeacher> implements IUserTeacherService {

    @Autowired
    private IAccountService accountService;

    @Override
    public UserTeacher getUserByUid(Long uid) {
        return this.getOne(Wrappers.<UserTeacher>lambdaQuery().eq(UserTeacher::getUid, uid));
    }

    @Override
    @Transactional
    public Boolean saveTeacher(UserTeacherAddDto dto) {
        UserTeacher userTeacher = this.getUserByUid(dto.getUid());
        UserTeacher entity = UserTeacherConvert.INSTANCE.convertAddDto(dto);
        entity.setId(userTeacher.getId());
        entity.setUid(userTeacher.getUid());
        return this.updateById(entity);
    }
}
