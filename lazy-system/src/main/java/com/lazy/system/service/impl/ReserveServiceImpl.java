package com.lazy.system.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.Reserve;
import com.lazy.system.mapper.ReserveMapper;
import com.lazy.system.service.IReserveService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 预约Service业务层处理
 *
 * @author yang
 * @date 2021-02-09
 */
@Service
public class ReserveServiceImpl extends ServiceImpl<ReserveMapper, Reserve> implements IReserveService {

}
