package com.lazy.system.service;

import com.lazy.system.domain.dto.UserTeacherAddDto;
import com.lazy.system.domain.entity.UserTeacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 教练Service接口
 *
 * @author yang
 * @date 2021-01-23
 */
public interface IUserTeacherService extends IService<UserTeacher> {

    UserTeacher getUserByUid(Long userUid);

    Boolean saveTeacher(UserTeacherAddDto dto);
}
