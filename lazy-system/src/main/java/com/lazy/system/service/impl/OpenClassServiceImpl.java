package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.OpenClass;
import com.lazy.system.mapper.OpenClassMapper;
import com.lazy.system.service.IOpenClassService;
import org.springframework.stereotype.Service;

/**
 * 公开课Service业务层处理
 *
 * @author yang
 * @date 2021-01-21
 */
@Service
public class OpenClassServiceImpl extends ServiceImpl<OpenClassMapper, OpenClass> implements IOpenClassService {

}
