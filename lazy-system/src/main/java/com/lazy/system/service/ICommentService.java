package com.lazy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lazy.system.domain.entity.Comment;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.List;

/**
 * 评论Service接口
 *
 * @author yang
 * @date 2021-01-21
 */
public interface ICommentService extends IService<Comment> {

    int commentCount(Long id, Integer code);

    Boolean deleteByIds(List<String> list);
}
