package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.common.core.redis.RedisCache;
import com.lazy.common.utils.TagUtil;
import com.lazy.system.domain.entity.GallupTag;
import com.lazy.system.domain.enums.TagTypeEnum;
import com.lazy.system.domain.query.applet.TagQuery;
import com.lazy.system.domain.vo.GallupTagChartVo;
import com.lazy.system.mapper.GallupTagMapper;
import com.lazy.system.service.IGallupTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * gallup标签Service业务层处理
 *
 * @author yang
 * @date 2021-01-02
 */
@Service
public class GallupTagServiceImpl extends ServiceImpl<GallupTagMapper, GallupTag> implements IGallupTagService {

    @Autowired
    private RedisCache redisCache;

    @Override
    public GallupTag getTagByCode(Integer code) {
        Object tag = redisCache.getCacheObject(code + "");
        if (tag == null) {
            List<GallupTag> list = this.list();
            list.forEach(gallupTag -> redisCache.setCacheObject(gallupTag.getCode() + "", gallupTag, 30, TimeUnit.DAYS));
            tag = redisCache.getCacheObject(code + "");
        }
        return (GallupTag) tag;
    }

    @Override
    public List<GallupTagChartVo> getTagChartByTagStr(String tagStr, TagQuery tagQuery) {
        if (StringUtils.isEmpty(tagStr)) {
            return new ArrayList<>();
        }
        List<Integer> tagList = TagUtil.getTagList(tagStr, tagQuery.getNum(), tagQuery.getSort() != 0);
        Map<Integer, List<GallupTag>> map = tagList.stream().map(this::getTagByCode).collect(Collectors.groupingBy(GallupTag::getType));
        return Arrays.stream(TagTypeEnum.values()).map(e -> buildGallupTagChartVo(map, e)).collect(Collectors.toList());
    }

    private GallupTagChartVo buildGallupTagChartVo(Map<Integer, List<GallupTag>> map, TagTypeEnum e) {
        GallupTagChartVo vo = new GallupTagChartVo();
        List<GallupTag> list = map.get(e.getCode());
        Integer count = 0;
        if (!CollectionUtils.isEmpty(list)) {
            count = list.size();
        }
        vo.setCount(count);
        vo.setType(e.getCode());
        vo.setName(e.getMsg());
        return vo;
    }

    @Override
    public List<GallupTag> getTagListByTagStr(String tagStr, Integer tagNum, Integer tagSort) {
        if (StringUtils.isEmpty(tagStr)) {
            return new ArrayList<>();
        }
        List<Integer> tagList = TagUtil.getTagList(tagStr, tagNum, tagSort != 0);
        return tagList.stream().map(this::getTagByCode).collect(Collectors.toList());
    }
}
