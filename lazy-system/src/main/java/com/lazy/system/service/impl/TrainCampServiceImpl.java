package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.TrainCamp;
import com.lazy.system.mapper.TrainCampMapper;
import com.lazy.system.service.ITrainCampService;
import org.springframework.stereotype.Service;

/**
 * 训练营Service业务层处理
 *
 * @author yang
 * @date 2021-01-21
 */
@Service
public class TrainCampServiceImpl extends ServiceImpl<TrainCampMapper, TrainCamp> implements ITrainCampService {

}
