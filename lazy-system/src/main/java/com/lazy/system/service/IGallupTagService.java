package com.lazy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lazy.system.domain.entity.GallupTag;
import com.lazy.system.domain.query.applet.TagQuery;
import com.lazy.system.domain.vo.GallupTagChartVo;

import java.util.List;

/**
 * gallup标签Service接口
 *
 * @author yang
 * @date 2021-01-02
 */
public interface IGallupTagService extends IService<GallupTag> {

    GallupTag getTagByCode(Integer code);

    List<GallupTagChartVo> getTagChartByTagStr(String tagStr, TagQuery tagQuery);

    List<GallupTag> getTagListByTagStr(String tags, Integer tagNum, Integer tagSort);
}
