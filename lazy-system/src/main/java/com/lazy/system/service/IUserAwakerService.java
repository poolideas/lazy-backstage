package com.lazy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lazy.system.domain.entity.UserAwaker;

/**
 * 觉醒者用户信息Service接口
 *
 * @author yang
 * @date 2021-01-02
 */
public interface IUserAwakerService extends IService<UserAwaker> {

    UserAwaker getUserByUid(Long userUid);
}
