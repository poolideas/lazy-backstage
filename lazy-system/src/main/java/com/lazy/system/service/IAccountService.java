package com.lazy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lazy.system.domain.dto.AccountImgAndNickNameDto;
import com.lazy.system.domain.entity.Account;
import com.lazy.system.domain.query.applet.AppletUserRegisterAndLoginParam;

/**
 * 账户Service接口
 *
 * @author yang
 * @date 2021-01-02
 */
public interface IAccountService extends IService<Account> {

    Long registerOrLogin(AppletUserRegisterAndLoginParam dto);

    Account register(String phoneNumber, String openid, Integer type);

    String getNameByUid(Long fromUid);

    AccountImgAndNickNameDto getImgAndNameByUid(Long uid);
}
