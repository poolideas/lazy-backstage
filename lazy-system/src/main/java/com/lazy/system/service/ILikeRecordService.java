package com.lazy.system.service;

import com.lazy.system.domain.entity.LikeRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 点赞Service接口
 *
 * @author yang
 * @date 2021-02-09
 */
public interface ILikeRecordService extends IService<LikeRecord> {

    int likeCount(Long id, Integer code);
}
