package com.lazy.system.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.AwakeFollow;
import com.lazy.system.mapper.AwakeFollowMapper;
import com.lazy.system.service.IAwakeFollowService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 我的关注的人Service业务层处理
 *
 * @author yang
 * @date 2021-01-23
 */
@Service
public class AwakeFollowServiceImpl extends ServiceImpl<AwakeFollowMapper, AwakeFollow> implements IAwakeFollowService {

}
