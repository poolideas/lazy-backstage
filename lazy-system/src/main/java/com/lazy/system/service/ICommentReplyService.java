package com.lazy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lazy.system.domain.entity.CommentReply;

/**
 * 评论回复Service接口
 *
 * @author yang
 * @date 2021-01-21
 */
public interface ICommentReplyService extends IService<CommentReply> {

    int commentReplyCount(Long id, Integer code);
}
