package com.lazy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lazy.system.domain.entity.Article;

/**
 * 文章Service接口
 *
 * @author yang
 * @date 2021-01-21
 */
public interface IArticleService extends IService<Article> {

}
