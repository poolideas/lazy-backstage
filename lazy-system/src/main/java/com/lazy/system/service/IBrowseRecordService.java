package com.lazy.system.service;

import com.lazy.system.domain.entity.BrowseRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 浏览Service接口
 *
 * @author yang
 * @date 2021-02-09
 */
public interface IBrowseRecordService extends IService<BrowseRecord> {

    int browseCount(Long id, Integer code);
}
