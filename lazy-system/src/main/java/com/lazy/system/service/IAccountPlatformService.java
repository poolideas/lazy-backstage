package com.lazy.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lazy.system.domain.entity.AccountPlatform;

/**
 * 第三方用户信息Service接口
 *
 * @author yang
 * @date 2021-01-02
 */
public interface IAccountPlatformService extends IService<AccountPlatform> {

}
