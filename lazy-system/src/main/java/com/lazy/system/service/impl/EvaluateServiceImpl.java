package com.lazy.system.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.Evaluate;
import com.lazy.system.mapper.EvaluateMapper;
import com.lazy.system.service.IEvaluateService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 预约评价Service业务层处理
 *
 * @author yang
 * @date 2021-02-09
 */
@Service
public class EvaluateServiceImpl extends ServiceImpl<EvaluateMapper, Evaluate> implements IEvaluateService {

}
