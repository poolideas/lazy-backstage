package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.AccountPlatform;
import com.lazy.system.mapper.AccountPlatformMapper;
import com.lazy.system.service.IAccountPlatformService;
import org.springframework.stereotype.Service;

/**
 * 第三方用户信息Service业务层处理
 *
 * @author yang
 * @date 2021-01-02
 */
@Service
public class AccountPlatformServiceImpl extends ServiceImpl<AccountPlatformMapper, AccountPlatform> implements IAccountPlatformService {

}
