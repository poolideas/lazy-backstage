package com.lazy.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lazy.system.domain.entity.LazyClass;
import com.lazy.system.mapper.LazyClassMapper;
import com.lazy.system.service.ILazyClassService;
import org.springframework.stereotype.Service;

/**
 * 课时Service业务层处理
 *
 * @author yang
 * @date 2021-01-21
 */
@Service
public class LazyClassServiceImpl extends ServiceImpl<LazyClassMapper, LazyClass> implements ILazyClassService {

}
