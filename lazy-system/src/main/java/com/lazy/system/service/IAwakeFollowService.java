package com.lazy.system.service;

import com.lazy.system.domain.entity.AwakeFollow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 我的关注的人Service接口
 *
 * @author yang
 * @date 2021-01-23
 */
public interface IAwakeFollowService extends IService<AwakeFollow> {

}
