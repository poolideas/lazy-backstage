package com.lazy.system.service;

import com.lazy.system.domain.entity.AwakeDiary;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 觉醒日记Service接口
 *
 * @author yang
 * @date 2021-01-23
 */
public interface IAwakeDiaryService extends IService<AwakeDiary> {

}
