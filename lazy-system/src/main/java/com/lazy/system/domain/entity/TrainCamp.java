package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 训练营对象 lazy_train_camp
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_train_camp")
public class TrainCamp extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 教练uid
     */
    private Long uid;
    /**
     * 标题
     */
    private String title;
    /**
     * 副标题
     */
    private String subTitle;
    /**
     * 介绍
     */
    private String content;
    /**
     * 封面
     */
    private String pic;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 开始时间 */
    private Date beginTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 结束时间 */
    private Date endTime;
    /**
     * 发布状态（0未发布 1已发布）
     */
    private Integer publishStatus;
    /**
     * 进行中状态（0未开始 1进行中 2已结束）
     */
    private Integer doingStatus;
}
