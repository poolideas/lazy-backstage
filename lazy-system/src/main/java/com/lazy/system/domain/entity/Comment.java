package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 评论对象 lazy_comment
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_comment")
public class Comment extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 被评论的主题ID
     */
    private Long topicId;
    /**
     * 主题的类型（1课程 2文章 3公开课 4训练营）
     */
    private Integer topicType;
    /**
     * 评论的内容
     */
    private String content;
    /**
     * 评论者UID
     */
    private Long fromUid;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 评论时间 */
    private Date commentTime;
}
