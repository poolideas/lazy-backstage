package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.CommentReplyAddDto;
import com.lazy.system.domain.entity.CommentReply;
import com.lazy.system.domain.vo.CommentReplyVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 评论回复类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface CommentReplyConvert {

    CommentReplyConvert INSTANCE = Mappers.getMapper(CommentReplyConvert.class);

    Page<CommentReplyVo> convertPage(Page<CommentReply> page);

    CommentReply convertAddDto(CommentReplyAddDto dto);

    List<CommentReplyVo> convertList(List<CommentReply> dto);

    CommentReplyVo convert(CommentReply dto);

}

