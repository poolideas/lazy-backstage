package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 训练营Vo对象 lazy_train_camp
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "TrainCampVo 训练营视图模型")
public class TrainCampVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @Excel(name = "${comment}", readConverterExp = "id")
    private Long id;
    private String idText;

    @ApiModelProperty(value = "教练uid")
    @Excel(name = "教练uid")
    private Long uid;

    @ApiModelProperty(value = "标题")
    @Excel(name = "标题")
    private String title;

    @ApiModelProperty(value = "副标题")
    @Excel(name = "副标题")
    private String subTitle;

    @ApiModelProperty(value = "介绍")
    @Excel(name = "介绍")
    private String content;

    @ApiModelProperty(value = "封面")
    @Excel(name = "封面")
    private String pic;

    @ApiModelProperty(value = "开始时间")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "结束时间")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "发布状态（0未发布 1已发布）")
    @Excel(name = "发布状态", readConverterExp = "0=未发布,1=已发布")
    private Integer publishStatus;
    private String publishStatusText;

    @ApiModelProperty(value = "进行中状态（0未开始 1进行中 2已结束）")
    @Excel(name = "进行中状态", readConverterExp = "0=未开始,1=进行中,2=已结束")
    private Integer doingStatus;
    private String doingStatusText;


    public void setPublishStatus(Integer publishStatus) {
        this.publishStatus = publishStatus;
        if (publishStatus == null) {
            return;
        }
        if (this.publishStatus == 0) {
            this.publishStatusText = "未发布" ;
        }
        if (this.publishStatus == 1) {
            this.publishStatusText = "已发布" ;
        }
    }

    public void setDoingStatus(Integer doingStatus) {
        this.doingStatus = doingStatus;
        if (doingStatus == null) {
            return;
        }
        if (this.doingStatus == 0) {
            this.doingStatusText = "未开始" ;
        }
        if (this.doingStatus == 1) {
            this.doingStatusText = "进行中" ;
        }
        if (this.doingStatus == 2) {
            this.doingStatusText = "已结束" ;
        }
    }
}
