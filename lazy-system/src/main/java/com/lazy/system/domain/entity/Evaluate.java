package com.lazy.system.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 预约评价对象 lazy_evaluate
 * 
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_evaluate")
public class Evaluate extends Entity implements Serializable {

private static final long serialVersionUID=1L;
    /** 用户唯一标识uid */
    private Long uid;
    /** 教练唯一标识uid */
    private Long teacherUid;
    /** 预约id */
    private Long reserveId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 评价时间 */
    private Date evaluateTime;
    /** 评价分数 */
    private Long evaluateScore;
    /** 评价内容 */
    private String content;
}
