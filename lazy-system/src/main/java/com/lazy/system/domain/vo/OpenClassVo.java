package com.lazy.system.domain.vo;

import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 公开课Vo对象 lazy_open_class
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "OpenClassVo 公开课视图模型")
public class OpenClassVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @Excel(name = "${comment}", readConverterExp = "id")
    private Long id;
    private String idText;

    @ApiModelProperty(value = "教练uid")
    @Excel(name = "教练uid")
    private Long uid;

    @ApiModelProperty(value = "标题")
    @Excel(name = "标题")
    private String title;

    @ApiModelProperty(value = "副标题")
    @Excel(name = "副标题")
    private String subTitle;

    @ApiModelProperty(value = "介绍")
    @Excel(name = "介绍")
    private String content;

    @ApiModelProperty(value = "封面")
    @Excel(name = "封面")
    private String pic;

    @ApiModelProperty(value = "发布状态（0未发布 1已发布）")
    @Excel(name = "发布状态", readConverterExp = "0=未发布,1=已发布")
    private Integer publishStatus;
    private String publishStatusText;


    public void setPublishStatus(Integer publishStatus) {
        this.publishStatus = publishStatus;
        if (publishStatus == null) {
            return;
        }
        if (this.publishStatus == 0) {
            this.publishStatusText = "未发布" ;
        }
        if (this.publishStatus == 1) {
            this.publishStatusText = "已发布" ;
        }
    }
}
