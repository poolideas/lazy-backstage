package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * gallup标签对象 lazy_gallup_tag
 *
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_gallup_tag")
public class GallupTag extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 编号1-34
     */
    private Long code;
    /**
     * 标签分类（1执行力 2影响力 3建立关系 4战略思维）
     */
    private Integer type;
    /**
     * 标签分类颜色（1紫色 2橙色 3蓝色 4绿色）
     */
    private Integer color;
    /**
     * 名称
     */
    private String name;
    /**
     * 怎么说
     */
    private String how;
    /**
     * 这样说
     */
    private String saySo;
    /**
     * 力量和优势
     */
    private String power;
    /**
     * 如果是你的突出才干
     */
    private String seniorTalent;
    /**
     * 如果是你的弱势才干
     */
    private String lowerTalent;
}
