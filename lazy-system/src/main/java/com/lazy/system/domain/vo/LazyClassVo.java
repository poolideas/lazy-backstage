package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 课时Vo对象 lazy_class
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "LazyClassVo 课时视图模型")
public class LazyClassVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @Excel(name = "${comment}", readConverterExp = "id")
    private Long id;
    private String idText;

    @ApiModelProperty(value = "训练营id/公开课id")
    @Excel(name = "训练营id/公开课id")
    private Long bizId;

    @ApiModelProperty(value = "课时")
    @Excel(name = "课时")
    private Long classPeriod;

    @ApiModelProperty(value = "类型（1训练营 2公开课）")
    @Excel(name = "类型", readConverterExp = "1=训练营,2=公开课")
    private Integer type;
    private String typeText;

    @ApiModelProperty(value = "课程类型（1视频 2音频 3文章）")
    @Excel(name = "课程类型", readConverterExp = "1=视频,2=音频,3=文章")
    private Integer classType;
    private String classTypeText;

    @ApiModelProperty(value = "副标题")
    @Excel(name = "副标题")
    private String subTitle;

    @ApiModelProperty(value = "文章内容")
    @Excel(name = "文章内容")
    private String content;

    @ApiModelProperty(value = "视频地址")
    @Excel(name = "视频地址")
    private String videoUrl;

    @ApiModelProperty(value = "音频地址")
    @Excel(name = "音频地址")
    private String audioUrl;

    @ApiModelProperty(value = "开始时间")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "结束时间")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "发布状态（0未发布 1已发布）")
    @Excel(name = "发布状态", readConverterExp = "0=未发布,1=已发布")
    private Integer publishStatus;
    private String publishStatusText;


    public void setType(Integer type) {
        this.type = type;
        if (type == null) {
            return;
        }
        if (this.type == 1) {
            this.typeText = "训练营" ;
        }
        if (this.type == 2) {
            this.typeText = "公开课" ;
        }
    }

    public void setClassType(Integer classType) {
        this.classType = classType;
        if (classType == null) {
            return;
        }
        if (this.classType == 1) {
            this.classTypeText = "视频" ;
        }
        if (this.classType == 2) {
            this.classTypeText = "音频" ;
        }
        if (this.classType == 3) {
            this.classTypeText = "文章" ;
        }
    }

    public void setPublishStatus(Integer publishStatus) {
        this.publishStatus = publishStatus;
        if (publishStatus == null) {
            return;
        }
        if (this.publishStatus == 0) {
            this.publishStatusText = "未发布" ;
        }
        if (this.publishStatus == 1) {
            this.publishStatusText = "已发布" ;
        }
    }
}
