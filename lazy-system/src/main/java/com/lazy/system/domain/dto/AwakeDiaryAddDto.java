package com.lazy.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 觉醒日记对象 lazy_awake_diary
 *
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AwakeDiaryAddDto 觉醒日记添加修改视图模型")
public class AwakeDiaryAddDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户id")
    private Long id;
    @ApiModelProperty(value = "标题")
    private String title;
    @ApiModelProperty(value = "才干 code用逗号分隔 例子（1,23,42）")
    private String tags;
    @ApiModelProperty(value = "内容")
    private String content;
    @ApiModelProperty(value = "是否公开（0不公开 1公开）")
    private Integer pub;
}
