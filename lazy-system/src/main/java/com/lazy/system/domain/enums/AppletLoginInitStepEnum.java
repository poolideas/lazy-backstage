package com.lazy.system.domain.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created by Administrator on 2020/5/20.
 */
@Getter
public enum AppletLoginInitStepEnum {

    LOGIN(1, "登陆"),
    REGISTER2(2, "注册保存标签"),
    REGISTER3(3, "注册保存期望工作"),

    ;

    private Integer code;

    private String msg;

    AppletLoginInitStepEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static String matchCode(Integer code) {
        return Arrays.stream(AppletLoginInitStepEnum.values())
                .filter(e -> e.getCode().equals(code))
                .map(AppletLoginInitStepEnum::getMsg)
                .findFirst().orElse(null);
    }


    public static Integer matchMsg(String msg) {
        return Arrays.stream(AppletLoginInitStepEnum.values())
                .filter(e -> e.getMsg().equals(msg))
                .map(AppletLoginInitStepEnum::getCode)
                .findFirst().orElse(null);
    }
}
