package com.lazy.system.domain.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 评论回复Vo对象 lazy_comment_reply
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "CommentReplyQuery 评论回复查询视图模型")
public class CommentReplyQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "被评论的主题ID")
    private Long topicId;

    @ApiModelProperty(value = "主题的类型（1课程 2文章 3公开课 4训练营）")
    private Integer topicType;

    @ApiModelProperty(value = "被回复的评论的ID")
    private Long commentId;

    @ApiModelProperty(value = "回复的内容")
    private String content;

    @ApiModelProperty(value = "进行回复动作的用户的UID")
    private Long fromUid;

    @ApiModelProperty(value = "被回复的用户UID")
    private Long toUid;

    @ApiModelProperty(value = "如果reply_type=1，就是comment_id; 如果reply_type=2，就是被回复的回复ID")
    private Long replyId;

    @ApiModelProperty(value = "回复类型（1评论 2回复）")
    private Integer replyType;

    @ApiModelProperty(value = "评论时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date commentTimeStart;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date commentTimeEnd;


}
