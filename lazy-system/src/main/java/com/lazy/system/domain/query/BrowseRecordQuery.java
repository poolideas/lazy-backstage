package com.lazy.system.domain.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 浏览Vo对象 lazy_browse_record
 *
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "BrowseRecordQuery 浏览查询视图模型")
public class BrowseRecordQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private Long id;

    @ApiModelProperty(value = "用户唯一标识uid")
    private Long uid;

    @ApiModelProperty(value = "被点赞的主题ID")
    private Long topicId;

    @ApiModelProperty(value = "主题的类型（1课程 2文章 3公开课 4训练营）")
    private Integer topicType;



}
