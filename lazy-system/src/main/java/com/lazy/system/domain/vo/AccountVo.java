package com.lazy.system.domain.vo;

import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 账户Vo对象 lazy_account
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AccountVo 账户视图模型")
public class AccountVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    @ApiModelProperty(value = "邮箱")
    @Excel(name = "邮箱")
    private String email;

    @ApiModelProperty(value = "手机号")
    @Excel(name = "手机号")
    private String phone;

    @ApiModelProperty(value = "用户名")
    @Excel(name = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    @Excel(name = "密码")
    private String password;

    @ApiModelProperty(value = "创建ip")
    @Excel(name = "创建ip")
    private String createIpAt;

    @ApiModelProperty(value = "最后一次登录ip")
    @Excel(name = "最后一次登录ip")
    private String lastLoginIpAt;

    @ApiModelProperty(value = "登录次数")
    @Excel(name = "登录次数")
    private Long loginTimes;

    @ApiModelProperty(value = "状态（0不可用 1可用）")
    @Excel(name = "状态", readConverterExp = "0=不可用,1=可用")
    private Integer status;
    private String statusText;

    @ApiModelProperty(value = "账户类型（1觉醒者 2教练）")
    @Excel(name = "账户类型", readConverterExp = "1=觉醒者,2=教练")
    private Integer type;
    private String typeText;


    public void setStatus(Integer status) {
        this.status = status;
        if (status == null) {
            return;
        }
        if (this.status == 0) {
            this.statusText = "不可用" ;
        }
        if (this.status == 1) {
            this.statusText = "可用" ;
        }
    }

    public void setType(Integer type) {
        this.type = type;
        if (type == null) {
            return;
        }
        if (this.type == 1) {
            this.typeText = "觉醒者" ;
        }
        if (this.type == 2) {
            this.typeText = "教练" ;
        }
    }
}
