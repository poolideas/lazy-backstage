package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.TrainCampAddDto;
import com.lazy.system.domain.entity.TrainCamp;
import com.lazy.system.domain.vo.TrainCampVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 训练营类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface TrainCampConvert {

    TrainCampConvert INSTANCE = Mappers.getMapper(TrainCampConvert.class);

    Page<TrainCampVo> convertPage(Page<TrainCamp> page);

    TrainCamp convertAddDto(TrainCampAddDto dto);

    List<TrainCampVo> convertList(List<TrainCamp> dto);

    TrainCampVo convert(TrainCamp dto);

}

