package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 预约评价Vo对象 lazy_evaluate
 *
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "EvaluateVo 预约评价视图模型")
public class EvaluateVo implements Serializable {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户id")
        @Excel(name = "${comment}" , readConverterExp = "用户id")
        private Long id;
        private String idText;
    
    @ApiModelProperty(value = "用户唯一标识uid")
        @Excel(name = "用户唯一标识uid")
        private Long uid;
    
    @ApiModelProperty(value = "教练唯一标识uid")
        @Excel(name = "教练唯一标识uid")
        private Long teacherUid;
    
    @ApiModelProperty(value = "预约id")
        @Excel(name = "预约id")
        private Long reserveId;
    
    @ApiModelProperty(value = "评价时间")
        @Excel(name = "评价时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private Date evaluateTime;
    
    @ApiModelProperty(value = "评价分数")
        @Excel(name = "评价分数")
        private Long evaluateScore;
    
    @ApiModelProperty(value = "评价内容")
        @Excel(name = "评价内容")
        private String content;
    



                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        }
