package com.lazy.system.domain.vo;

import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 第三方用户信息Vo对象 lazy_account_platform
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AccountPlatformVo 第三方用户信息视图模型")
public class AccountPlatformVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    @ApiModelProperty(value = "账号id")
    @Excel(name = "账号id")
    private Long uid;

    @ApiModelProperty(value = "平台id")
    @Excel(name = "平台id")
    private String platformId;

    @ApiModelProperty(value = "平台access_token")
    @Excel(name = "平台access_token")
    private String platformToken;

    @ApiModelProperty(value = "平台类型 （0未知 1小程序）")
    @Excel(name = "平台类型 ", readConverterExp = "0=未知,1=小程序")
    private Integer type;
    private String typeText;

    @ApiModelProperty(value = "昵称")
    @Excel(name = "昵称")
    private String nickname;

    @ApiModelProperty(value = "头像")
    @Excel(name = "头像")
    private String avatar;


    public void setType(Integer type) {
        this.type = type;
        if (type == null) {
            return;
        }
        if (this.type == 0) {
            this.typeText = "未知" ;
        }
        if (this.type == 1) {
            this.typeText = "小程序" ;
        }
    }
}
