package com.lazy.system.domain.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccountImgAndNickNameDto implements Serializable {
    private String name;
    private String avatar;
    private String phone;
}
