package com.lazy.system.domain.vo;

import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 觉醒者用户信息Vo对象 lazy_user_awaker
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "UserAwakerVo 觉醒者用户信息视图模型")
public class UserAwakerVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "账号id")
    @Excel(name = "账号id")
    private Long uid;

    @ApiModelProperty(value = "手机号")
    @Excel(name = "手机号")
    private String phone;

    @ApiModelProperty(value = "昵称")
    @Excel(name = "昵称")
    private String nickname;

    @ApiModelProperty(value = "姓名")
    @Excel(name = "姓名")
    private String name;

    @ApiModelProperty(value = "头像(相对路径)")
    @Excel(name = "头像(相对路径)")
    private String avatar;

    @ApiModelProperty(value = "性别（0女 1男 2未知）")
    @Excel(name = "性别", readConverterExp = "0=女,1=男,2=未知")
    private Integer gender;
    private String genderText;

    @ApiModelProperty(value = "角色（0:普通用户 1:vip）")
    @Excel(name = "角色", readConverterExp = "0=:普通用户,1=:vip")
    private Integer vip;
    private String vipText;

    @ApiModelProperty(value = "gallup标签")
    @Excel(name = "gallup标签")
    private String tags;

    @ApiModelProperty(value = "目前从事的工作")
    @Excel(name = "目前从事的工作")
    private String job;

    @ApiModelProperty(value = "期望从事的工作")
    @Excel(name = "期望从事的工作")
    private String wishJob;


    public void setGender(Integer gender) {
        this.gender = gender;
        if (this.gender == 0) {
            this.genderText = "女" ;
        }
        if (this.gender == 1) {
            this.genderText = "男" ;
        }
        if (this.gender == 2) {
            this.genderText = "未知" ;
        }
    }

    public void setVip(Integer vip) {
        this.vip = vip;
        if (vip == null) {
            return;
        }
        if (this.vip == 0) {
            this.vipText = "普通用户" ;
        }
        if (this.vip == 1) {
            this.vipText = "vip" ;
        }
    }
}
