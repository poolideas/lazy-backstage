package com.lazy.system.domain.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 第三方用户信息Vo对象 lazy_account_platform
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AccountPlatformQuery 第三方用户信息查询视图模型")
public class AccountPlatformQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增id")
    private Long id;

    @ApiModelProperty(value = "账号id")
    private Long uid;

    @ApiModelProperty(value = "平台id")
    private String platformId;

    @ApiModelProperty(value = "平台access_token")
    private String platformToken;

    @ApiModelProperty(value = "平台类型 （0未知 1小程序）")
    private Integer type;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "头像")
    private String avatar;


}
