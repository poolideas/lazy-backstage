package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.AwakeDiaryAddDto;
import com.lazy.system.domain.entity.AwakeDiary;
import com.lazy.system.domain.vo.AwakeDiaryVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 觉醒日记类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface AwakeDiaryConvert {

    AwakeDiaryConvert INSTANCE = Mappers.getMapper(AwakeDiaryConvert.class);

    Page<AwakeDiaryVo> convertPage(Page<AwakeDiary> page);

    AwakeDiary convertAddDto(AwakeDiaryAddDto dto);

    List<AwakeDiaryVo> convertList(List<AwakeDiary> dto);

    AwakeDiaryVo convert(AwakeDiary dto);

}

