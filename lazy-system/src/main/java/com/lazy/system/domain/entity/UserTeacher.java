package com.lazy.system.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 教练对象 lazy_user_teacher
 * 
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_user_teacher")
public class UserTeacher extends Entity implements Serializable {

private static final long serialVersionUID=1L;
    /** 账号id */
    private Long uid;
    /** 昵称 */
    private String nickname;
    /** 姓名 */
    private String name;
    /** 头像 */
    private String avatar;
    /** 性别（0女 1男 2未知） */
    private Integer gender;
    /** 证书 */
    private String certificate;
}
