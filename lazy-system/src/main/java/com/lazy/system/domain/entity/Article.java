package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 文章对象 lazy_article
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_article")
public class Article extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户uid
     */
    private Long uid;
    /**
     * 标题
     */
    private String title;
    /**
     * 是否公开（0不公开 1公开）
     */
    private Integer pub;
    /**
     * 内容
     */
    private String content;
    /**
     * 图片
     */
    private String urls;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 发布时间 */
    private Date publishTime;
}
