package com.lazy.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 账户对象 lazy_account
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AccountAddDto 账户添加修改视图模型")
public class AccountAddDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "账号id")
    private Long id;
    @ApiModelProperty(value = "邮箱")
    private String email;
    @ApiModelProperty(value = "手机号")
    private String phone;
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "创建ip")
    private String createIpAt;
    @ApiModelProperty(value = "最后一次登录ip")
    private String lastLoginIpAt;
    @ApiModelProperty(value = "登录次数")
    private Long loginTimes;
    @ApiModelProperty(value = "状态（0不可用 1可用）")
    private Integer status;
    @ApiModelProperty(value = "账户类型（1觉醒者 2教练）")
    private Integer type;
}
