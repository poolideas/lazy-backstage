package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.AwakeFollowAddDto;
import com.lazy.system.domain.entity.AwakeFollow;
import com.lazy.system.domain.vo.AwakeFollowVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 我的关注的人类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface AwakeFollowConvert {

    AwakeFollowConvert INSTANCE = Mappers.getMapper(AwakeFollowConvert.class);

    Page<AwakeFollowVo> convertPage(Page<AwakeFollow> page);
    AwakeFollow convertAddDto(AwakeFollowAddDto dto);
    List<AwakeFollowVo> convertList(List<AwakeFollow> dto);
    AwakeFollowVo convert(AwakeFollow dto);

}

