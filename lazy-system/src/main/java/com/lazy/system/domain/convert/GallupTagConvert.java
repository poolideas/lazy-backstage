package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.GallupTagAddDto;
import com.lazy.system.domain.entity.GallupTag;
import com.lazy.system.domain.vo.GallupTagVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * gallup标签类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface GallupTagConvert {

    GallupTagConvert INSTANCE = Mappers.getMapper(GallupTagConvert.class);

    Page<GallupTagVo> convertPage(Page<GallupTag> page);

    GallupTag convertAddDto(GallupTagAddDto dto);

    List<GallupTagVo> convertList(List<GallupTag> dto);

    GallupTagVo convert(GallupTag dto);

}

