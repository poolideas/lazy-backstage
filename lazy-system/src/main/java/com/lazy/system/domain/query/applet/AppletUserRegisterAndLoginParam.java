package com.lazy.system.domain.query.applet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AppletUserRegisterAndLoginParam 小程序登陆入参 视图模型")
public class AppletUserRegisterAndLoginParam implements Serializable {

    @ApiModelProperty(value = "手机号")
    private String phone;
    @ApiModelProperty(value = "昵称")
    private String nickName;
    @ApiModelProperty(value = "头像")
    private String avatarUrl;
    @ApiModelProperty(value = "openid")
    private String openid;
    @ApiModelProperty(value = "用户类型 1觉醒者 2教练")
    private Integer type;
    @ApiModelProperty(value = "数量 查询前5 或前10标签")
    private Integer num;
    @ApiModelProperty(value = "排序 0倒序 1正序")
    private Integer sort = 1;

}
