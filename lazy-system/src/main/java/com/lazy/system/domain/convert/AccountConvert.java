package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.AccountAddDto;
import com.lazy.system.domain.entity.Account;
import com.lazy.system.domain.vo.AccountVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 账户类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface AccountConvert {

    AccountConvert INSTANCE = Mappers.getMapper(AccountConvert.class);

    Page<AccountVo> convertPage(Page<Account> page);

    Account convertAddDto(AccountAddDto dto);

    List<AccountVo> convertList(List<Account> dto);

    AccountVo convert(Account dto);

}

