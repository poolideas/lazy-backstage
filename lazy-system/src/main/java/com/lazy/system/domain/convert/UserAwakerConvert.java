package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.UserAwakerAddDto;
import com.lazy.system.domain.entity.UserAwaker;
import com.lazy.system.domain.vo.UserAwakerVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;


/**
 * 觉醒者用户信息类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface UserAwakerConvert {

    UserAwakerConvert INSTANCE = Mappers.getMapper(UserAwakerConvert.class);

    Page<UserAwakerVo> convertPage(Page<UserAwaker> page);

    UserAwaker convertAddDto(UserAwakerAddDto dto);

    List<UserAwakerVo> convertList(List<UserAwaker> dto);

    UserAwakerVo convert(UserAwaker dto);

}

