package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.LikeRecordAddDto;
import com.lazy.system.domain.entity.LikeRecord;
import com.lazy.system.domain.vo.LikeRecordVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 点赞类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface LikeRecordConvert {

    LikeRecordConvert INSTANCE = Mappers.getMapper(LikeRecordConvert.class);

    Page<LikeRecordVo> convertPage(Page<LikeRecord> page);
    LikeRecord convertAddDto(LikeRecordAddDto dto);
    List<LikeRecordVo> convertList(List<LikeRecord> dto);
    LikeRecordVo convert(LikeRecord dto);

}

