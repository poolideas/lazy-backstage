package com.lazy.system.domain.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created by Administrator on 2020/5/20.
 */
@Getter
public enum ReserveStatusEnum {

    APPLY(1, "课程"),
    ACCEPT(2, "接受"),
    COMPLETE(3, "完成"),
    REFUSE(4, "拒绝"),
    CANCEL(5, "取消");

    private Integer code;

    private String msg;

    ReserveStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static String matchCode(Integer code) {
        return Arrays.stream(ReserveStatusEnum.values())
                .filter(e -> e.getCode().equals(code))
                .map(ReserveStatusEnum::getMsg)
                .findFirst().orElse(null);
    }


    public static Integer matchMsg(String msg) {
        return Arrays.stream(ReserveStatusEnum.values())
                .filter(e -> e.getMsg().equals(msg))
                .map(ReserveStatusEnum::getCode)
                .findFirst().orElse(null);
    }
}
