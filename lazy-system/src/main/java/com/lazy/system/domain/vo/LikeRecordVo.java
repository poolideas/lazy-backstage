package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 点赞Vo对象 lazy_like_record
 *
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "LikeRecordVo 点赞视图模型")
public class LikeRecordVo implements Serializable {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户id")
        @Excel(name = "${comment}" , readConverterExp = "用户id")
        private Long id;
        private String idText;
    
    @ApiModelProperty(value = "用户唯一标识uid")
        @Excel(name = "用户唯一标识uid")
        private Long uid;
    
    @ApiModelProperty(value = "被点赞的主题ID")
        @Excel(name = "被点赞的主题ID")
        private Long topicId;
    
    @ApiModelProperty(value = "主题的类型（1课程 2文章 3公开课 4训练营）")
        @Excel(name = "主题的类型" , readConverterExp = "1=课程,2=文章,3=公开课,4=训练营")
        private Integer topicType;
        private String topicTypeText;
    
    @ApiModelProperty(value = "点赞时间")
        @Excel(name = "点赞时间" , width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private Date likeTime;
    



                                                                                                                                                                                    public void setTopicType(Integer topicType) {
                this.topicType = topicType;
                if (topicType == null) {
                    return;
                }
                                    if (this.topicType==1) {
                        this.topicTypeText = "课程";
                    }
                                    if (this.topicType==2) {
                        this.topicTypeText = "文章";
                    }
                                    if (this.topicType==3) {
                        this.topicTypeText = "公开课";
                    }
                                    if (this.topicType==4) {
                        this.topicTypeText = "训练营";
                    }
                            }
                                                                                                                                                                                                                                        }
