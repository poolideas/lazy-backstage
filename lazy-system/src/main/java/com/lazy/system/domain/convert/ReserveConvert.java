package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.ReserveAddDto;
import com.lazy.system.domain.entity.Reserve;
import com.lazy.system.domain.vo.ReserveVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 预约类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface ReserveConvert {

    ReserveConvert INSTANCE = Mappers.getMapper(ReserveConvert.class);

    Page<ReserveVo> convertPage(Page<Reserve> page);
    Reserve convertAddDto(ReserveAddDto dto);
    List<ReserveVo> convertList(List<Reserve> dto);
    ReserveVo convert(Reserve dto);

}

