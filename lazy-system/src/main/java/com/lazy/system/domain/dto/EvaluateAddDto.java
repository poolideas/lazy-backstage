package com.lazy.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 预约评价对象 lazy_evaluate
 * 
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "EvaluateAddDto 预约评价添加修改视图模型")
public class EvaluateAddDto implements Serializable {

private static final long serialVersionUID=1L;
@ApiModelProperty(value = "用户id")
    private Long id;
@ApiModelProperty(value = "用户唯一标识uid")
    private Long uid;
@ApiModelProperty(value = "教练唯一标识uid")
    private Long teacherUid;
@ApiModelProperty(value = "预约id")
    private Long reserveId;
@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
@ApiModelProperty(value = "评价时间")
    private Date evaluateTime;
@ApiModelProperty(value = "评价分数")
    private Long evaluateScore;
@ApiModelProperty(value = "评价内容")
    private String content;
}
