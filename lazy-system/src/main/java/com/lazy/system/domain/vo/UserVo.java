package com.lazy.system.domain.vo;

import com.lazy.system.domain.entity.UserTeacher;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * LoginInitVo 小程序登陆初始化参数 视图模型
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "UserVo 小程序登陆初始化参数 视图模型")
public class UserVo {

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "觉醒者状态（1登陆 2注册第二步 3注册第三部）")
    private Integer status;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "觉醒者信息")
    private UserAwakerVo UserAwakerVo;

    @ApiModelProperty(value = "教练信息")
    private UserTeacherVo userTeacherVo;

    @ApiModelProperty(value = "标签列表")
    private List<GallupTagVo> tags;

    @ApiModelProperty(value = "账户类型 1觉醒者 2教练")
    private Integer type;
}
