package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.EvaluateAddDto;
import com.lazy.system.domain.entity.Evaluate;
import com.lazy.system.domain.vo.EvaluateVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 预约评价类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface EvaluateConvert {

    EvaluateConvert INSTANCE = Mappers.getMapper(EvaluateConvert.class);

    Page<EvaluateVo> convertPage(Page<Evaluate> page);
    Evaluate convertAddDto(EvaluateAddDto dto);
    List<EvaluateVo> convertList(List<Evaluate> dto);
    EvaluateVo convert(Evaluate dto);

}

