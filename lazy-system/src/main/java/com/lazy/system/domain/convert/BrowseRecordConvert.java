package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.BrowseRecordAddDto;
import com.lazy.system.domain.entity.BrowseRecord;
import com.lazy.system.domain.vo.BrowseRecordVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 浏览类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface BrowseRecordConvert {

    BrowseRecordConvert INSTANCE = Mappers.getMapper(BrowseRecordConvert.class);

    Page<BrowseRecordVo> convertPage(Page<BrowseRecord> page);
    BrowseRecord convertAddDto(BrowseRecordAddDto dto);
    List<BrowseRecordVo> convertList(List<BrowseRecord> dto);
    BrowseRecordVo convert(BrowseRecord dto);

}

