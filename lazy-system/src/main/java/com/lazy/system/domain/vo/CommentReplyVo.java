package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 评论回复Vo对象 lazy_comment_reply
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "CommentReplyVo 评论回复视图模型")
public class CommentReplyVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @Excel(name = "${comment}", readConverterExp = "id")
    private Long id;
    private String idText;

    @ApiModelProperty(value = "被评论的主题ID")
    @Excel(name = "被评论的主题ID")
    private Long topicId;

    @ApiModelProperty(value = "主题的类型（1课程 2文章 3公开课 4训练营）")
    @Excel(name = "主题的类型", readConverterExp = "1=课程,2=文章,3=公开课,4=训练营")
    private Integer topicType;
    private String topicTypeText;

    @ApiModelProperty(value = "被回复的评论的ID")
    @Excel(name = "被回复的评论的ID")
    private Long commentId;

    @ApiModelProperty(value = "回复的内容")
    @Excel(name = "回复的内容")
    private String content;

    @ApiModelProperty(value = "进行回复动作的用户的UID")
    @Excel(name = "进行回复动作的用户的UID")
    private Long fromUid;

    @ApiModelProperty(value = "进行回复动作的用户名称")
    private String fromName;

    @ApiModelProperty(value = "被回复的用户UID")
    @Excel(name = "被回复的用户UID")
    private Long toUid;

    @ApiModelProperty(value = "被回复的用户名称")
    private String toName;

    @ApiModelProperty(value = "如果reply_type=1，就是comment_id; 如果reply_type=2，就是被回复的回复ID")
    @Excel(name = "如果reply_type=1，就是comment_id; 如果reply_type=2，就是被回复的回复ID")
    private Long replyId;

    @ApiModelProperty(value = "回复类型（1评论 2回复）")
    @Excel(name = "回复类型", readConverterExp = "1=评论,2=回复")
    private Integer replyType;
    private String replyTypeText;

    @ApiModelProperty(value = "评论时间")
    @Excel(name = "评论时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date commentTime;


    public void setTopicType(Integer topicType) {
        this.topicType = topicType;
        if (topicType == null) {
            return;
        }
        if (this.topicType == 1) {
            this.topicTypeText = "课程" ;
        }
        if (this.topicType == 2) {
            this.topicTypeText = "文章" ;
        }
        if (this.topicType == 3) {
            this.topicTypeText = "公开课" ;
        }
        if (this.topicType == 4) {
            this.topicTypeText = "训练营" ;
        }
    }

    public void setReplyType(Integer replyType) {
        this.replyType = replyType;
        if (replyType == null) {
            return;
        }
        if (this.replyType == 1) {
            this.replyTypeText = "评论" ;
        }
        if (this.replyType == 2) {
            this.replyTypeText = "回复" ;
        }
    }
}
