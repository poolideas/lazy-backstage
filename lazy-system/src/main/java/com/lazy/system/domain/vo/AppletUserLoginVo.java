package com.lazy.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "AppletUserLoginVo 小程序用户登陆视图模型")
public class AppletUserLoginVo {

    @ApiModelProperty(value = "token", required = false)
    private String token;
    private Long id;
    private String nickName;
    private String avatarUrl;
}
