package com.lazy.system.domain.query;

import lombok.Data;

import java.io.Serializable;

@Data
public class AppletLoginDto implements Serializable {
    private String code;
    private Long inviterId;
    private Long activityId;
    private String inviterLink;
}
