package com.lazy.system.domain.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 我的关注的人Vo对象 lazy_awake_follow
 * 
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AwakeFollowQuery 我的关注的人查询视图模型")
public class AwakeFollowQuery implements Serializable {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户id")
        private Long id;
    
    @ApiModelProperty(value = "用户唯一标识uid")
        private Long uid;
    
    @ApiModelProperty(value = "我关注的人的uid")
        private Long followUid;
    
    @ApiModelProperty(value = "关注的人类型（1觉醒者 2教练）")
        private Integer type;
    
    @ApiModelProperty(value = "关注时间")
        @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date followTimeStart;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date followTimeEnd;
    


}
