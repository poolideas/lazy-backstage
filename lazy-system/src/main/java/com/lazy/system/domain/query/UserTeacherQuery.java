package com.lazy.system.domain.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 教练Vo对象 lazy_user_teacher
 * 
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "UserTeacherQuery 教练查询视图模型")
public class UserTeacherQuery implements Serializable {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户id")
        private Long id;
    
    @ApiModelProperty(value = "账号id")
        private Long uid;
    
    @ApiModelProperty(value = "昵称")
        private String nickname;
    
    @ApiModelProperty(value = "姓名")
        private String name;
    
    @ApiModelProperty(value = "头像")
        private String avatar;
    
    @ApiModelProperty(value = "性别（0女 1男 2未知）")
        private Integer gender;
    
    @ApiModelProperty(value = "证书")
        private String certificate;
    


}
