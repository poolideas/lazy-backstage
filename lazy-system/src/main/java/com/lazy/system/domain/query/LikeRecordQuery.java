package com.lazy.system.domain.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 点赞Vo对象 lazy_like_record
 * 
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "LikeRecordQuery 点赞查询视图模型")
public class LikeRecordQuery implements Serializable {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户id")
        private Long id;
    
    @ApiModelProperty(value = "用户唯一标识uid")
        private Long uid;
    
    @ApiModelProperty(value = "被点赞的主题ID")
        private Long topicId;
    
    @ApiModelProperty(value = "主题的类型（1课程 2文章 3公开课 4训练营）")
        private Integer topicType;
    
    @ApiModelProperty(value = "点赞时间")
        @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date likeTimeStart;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date likeTimeEnd;
    


}
