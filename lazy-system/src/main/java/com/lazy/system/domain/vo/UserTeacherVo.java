package com.lazy.system.domain.vo;

import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 教练Vo对象 lazy_user_teacher
 *
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "UserTeacherVo 教练视图模型")
public class UserTeacherVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @Excel(name = "${comment}", readConverterExp = "用户id")
    private Long id;
    private String idText;

    @ApiModelProperty(value = "账号id")
    @Excel(name = "账号id")
    private Long uid;

    @ApiModelProperty(value = "昵称")
    @Excel(name = "昵称")
    private String nickname;

    @ApiModelProperty(value = "姓名")
    @Excel(name = "姓名")
    private String name;

    @ApiModelProperty(value = "手机号")
    @Excel(name = "手机号")
    private String phone;

    @ApiModelProperty(value = "头像")
    @Excel(name = "头像")
    private String avatar;

    @ApiModelProperty(value = "性别（0女 1男 2未知）")
    @Excel(name = "性别", readConverterExp = "0=女,1=男,2=未知")
    private Integer gender;
    private String genderText;

    @ApiModelProperty(value = "证书")
    @Excel(name = "证书")
    private String certificate;


    public void setGender(Integer gender) {
        this.gender = gender;
        if (gender == null) {
            return;
        }
        if (this.gender == 0) {
            this.genderText = "女" ;
        }
        if (this.gender == 1) {
            this.genderText = "男" ;
        }
        if (this.gender == 2) {
            this.genderText = "未知" ;
        }
    }
}
