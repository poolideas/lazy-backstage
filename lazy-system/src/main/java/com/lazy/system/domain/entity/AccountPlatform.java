package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 第三方用户信息对象 lazy_account_platform
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_account_platform")
public class AccountPlatform extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 账号id
     */
    private Long uid;
    /**
     * 平台id
     */
    private String platformId;
    /**
     * 平台access_token
     */
    private String platformToken;
    /**
     * 平台类型 （0未知 1小程序）
     */
    private Integer type;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 头像
     */
    private String avatar;
}
