package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.AccountPlatformAddDto;
import com.lazy.system.domain.entity.AccountPlatform;
import com.lazy.system.domain.vo.AccountPlatformVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 第三方用户信息类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface AccountPlatformConvert {

    AccountPlatformConvert INSTANCE = Mappers.getMapper(AccountPlatformConvert.class);

    Page<AccountPlatformVo> convertPage(Page<AccountPlatform> page);

    AccountPlatform convertAddDto(AccountPlatformAddDto dto);

    List<AccountPlatformVo> convertList(List<AccountPlatform> dto);

    AccountPlatformVo convert(AccountPlatform dto);

}

