package com.lazy.system.domain.query.applet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "TagQuery 查标签 标签维度入参 视图模型")
public class TagQuery {

    @ApiModelProperty(value = "数量 查询前5 或前10标签")
    private Integer num = 5;
    @ApiModelProperty(value = "排序 0倒序 1正序")
    private Integer sort = 1;

}
