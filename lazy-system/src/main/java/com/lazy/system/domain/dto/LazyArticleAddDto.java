package com.lazy.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章对象 lazy_article
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "LazyArticleAddDto 文章添加修改视图模型")
public class LazyArticleAddDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "用户uid")
    private Long uid;
    @ApiModelProperty(value = "标题")
    private String title;
    @ApiModelProperty(value = "是否公开（0不公开 1公开）")
    private Integer pub;
    @ApiModelProperty(value = "内容")
    private String content;
    @ApiModelProperty(value = "图片")
    private String urls;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发布时间")
    private Date publishTime;
}
