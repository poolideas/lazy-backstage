package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 我的关注的人Vo对象 lazy_awake_follow
 *
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AwakeFollowVo 我的关注的人视图模型")
public class AwakeFollowVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @Excel(name = "${comment}", readConverterExp = "用户id")
    private Long id;
    private String idText;

    @ApiModelProperty(value = "用户唯一标识uid")
    @Excel(name = "用户唯一标识uid")
    private Long uid;

    @ApiModelProperty(value = "我关注的人的uid")
    @Excel(name = "我关注的人的uid")
    private Long followUid;
    @ApiModelProperty(value = "我关注的人的名字")
    private String followName;


    @ApiModelProperty(value = "关注的人类型（1觉醒者 2教练）")
    @Excel(name = "关注的人类型", readConverterExp = "1=觉醒者,2=教练")
    private Integer type;
    private String typeText;

    @ApiModelProperty(value = "关注时间")
    @Excel(name = "关注时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date followTime;


    public void setType(Integer type) {
        this.type = type;
        if (type == null) {
            return;
        }
        if (this.type == 1) {
            this.typeText = "觉醒者" ;
        }
        if (this.type == 2) {
            this.typeText = "教练" ;
        }
    }
}
