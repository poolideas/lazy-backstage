package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.LazyClassAddDto;
import com.lazy.system.domain.entity.LazyClass;
import com.lazy.system.domain.vo.LazyClassVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 课时类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface LazyClassConvert {

    LazyClassConvert INSTANCE = Mappers.getMapper(LazyClassConvert.class);

    Page<LazyClassVo> convertPage(Page<LazyClass> page);

    LazyClass convertAddDto(LazyClassAddDto dto);

    List<LazyClassVo> convertList(List<LazyClass> dto);

    LazyClassVo convert(LazyClass dto);

}

