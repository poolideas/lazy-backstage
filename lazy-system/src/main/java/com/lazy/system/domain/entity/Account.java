package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 账户对象 lazy_account
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_account")
public class Account extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 创建ip
     */
    private String createIpAt;
    /**
     * 最后一次登录ip
     */
    private String lastLoginIpAt;
    /**
     * 登录次数
     */
    private Long loginTimes;
    /**
     * 状态（0不可用 1可用）
     */
    private Integer status;
    /**
     * 账户类型（1觉醒者 2教练）
     */
    private Integer type;
}
