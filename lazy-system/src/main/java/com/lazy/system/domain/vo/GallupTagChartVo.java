package com.lazy.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * gallup标签Vo对象 lazy_gallup_tag
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "GallupTagVo gallup标签视图模型")
public class GallupTagChartVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标签分类（1执行力 2影响力 3建立关系 4战略思维）")
    private Integer type;
    private String typeText;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "数量")
    private Integer count;


    public void setType(Integer type) {
        this.type = type;
        if (type == null) {
            return;
        }
        if (this.type == 1) {
            this.typeText = "执行力" ;
        }
        if (this.type == 2) {
            this.typeText = "影响力" ;
        }
        if (this.type == 3) {
            this.typeText = "建立关系" ;
        }
        if (this.type == 4) {
            this.typeText = "战略思维" ;
        }
    }


}
