package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * @author mark
 */
@Data
public class Entity {

    @TableId(value = "id", type = IdType.AUTO)
    public Long id;

    @TableField("creator")
    protected String creator;

    @TableField("create_time")
    protected Date createTime;

    @TableField("updater")
    private String updater;

    @TableField("update_time")
    protected Date updateTime;

    //    @TableLogic
    @TableField("delete_flag")
    protected Integer deleteFlag;


}
