package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 觉醒日记Vo对象 lazy_awake_diary
 *
 * @author yang
 * @date 2021-01-23
 */
@Data
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AwakeDiaryDoneVo 觉醒日记完成情况视图模型")
public class AwakeDiaryDoneVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "觉醒日记完成日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date date;

    @ApiModelProperty(value = "完成情况（0未完成 1完成）")
    private Integer done;

}
