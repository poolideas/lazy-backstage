package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 预约Vo对象 lazy_reserve
 *
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "ReserveVo 预约视图模型")
public class ReserveVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @Excel(name = "${comment}", readConverterExp = "用户id")
    private Long id;

    @ApiModelProperty(value = "用户唯一标识uid")
    @Excel(name = "用户唯一标识uid")
    private Long uid;
    @ApiModelProperty(value = "姓名")
    private String name;
    @ApiModelProperty(value = "头像")
    private String avatar;
    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "教练唯一标识uid")
    @Excel(name = "教练唯一标识uid")
    private Long teacherUid;
    @ApiModelProperty(value = "姓名")
    private String teacherName;
    @ApiModelProperty(value = "头像")
    private String teacherAvatar;
    @ApiModelProperty(value = "手机号")
    private String teacherPhone;

    @ApiModelProperty(value = "服务时间")
    @Excel(name = "服务时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date serviceTime;

    @ApiModelProperty(value = "状态（1申请中 2已接受 3已拒绝 4已完成）")
    @Excel(name = "状态", readConverterExp = "1=申请中,2=已接受,3=已拒绝,4=已完成")
    private Long status;
    private String statusText;

    @ApiModelProperty(value = "预约要求")
    @Excel(name = "预约要求")
    private String demand;


    public void setStatus(Long status) {
        this.status = status;
        if (status == null) {
            return;
        }
        if (this.status == 1) {
            this.statusText = "申请中";
        }
        if (this.status == 2) {
            this.statusText = "已接受";
        }
        if (this.status == 3) {
            this.statusText = "已拒绝";
        }
        if (this.status == 4) {
            this.statusText = "已完成";
        }
        if (this.status == 5) {
            this.statusText = "已取消";
        }
    }
}
