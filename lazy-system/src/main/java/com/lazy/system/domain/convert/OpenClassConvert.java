package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.OpenClassAddDto;
import com.lazy.system.domain.entity.OpenClass;
import com.lazy.system.domain.vo.OpenClassVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 公开课类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface OpenClassConvert {

    OpenClassConvert INSTANCE = Mappers.getMapper(OpenClassConvert.class);

    Page<OpenClassVo> convertPage(Page<OpenClass> page);

    OpenClass convertAddDto(OpenClassAddDto dto);

    List<OpenClassVo> convertList(List<OpenClass> dto);

    OpenClassVo convert(OpenClass dto);

}

