package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 觉醒日记对象 lazy_awake_diary
 *
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_awake_diary")
public class AwakeDiary extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户唯一标识uid
     */
    private Long uid;
    /**
     * 标题
     */
    private String title;
    /**
     * 才干
     */
    private String tags;
    /**
     * 内容
     */
    private String content;
    /**
     * 是否公开（0不公开 1公开）
     */
    private Integer pub;
}
