package com.lazy.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 觉醒者用户信息对象 lazy_user_awaker
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "UserAwakerAddDto 觉醒者用户信息添加修改视图模型")
public class UserAwakerAddDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户id")
    private Long id;
    @ApiModelProperty(value = "账号id")
    private Long uid;
    @ApiModelProperty(value = "昵称")
    private String nickname;
    @ApiModelProperty(value = "姓名")
    private String name;
    @ApiModelProperty(value = "头像(相对路径)")
    private String avatar;
    @ApiModelProperty(value = "性别（0女 1男 2未知）")
    private Integer gender;
    @ApiModelProperty(value = "角色（0:普通用户 1:vip）")
    private Integer vip;
    @ApiModelProperty(value = "gallup标签")
    private String tags;
    @ApiModelProperty(value = "目前从事的工作")
    private String job;
    @ApiModelProperty(value = "期望从事的工作")
    private String wishJob;
}
