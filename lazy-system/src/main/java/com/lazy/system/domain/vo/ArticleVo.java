package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 文章Vo对象 lazy_article
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "ArticleVo 文章视图模型")
public class ArticleVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @Excel(name = "${comment}", readConverterExp = "id")
    private Long id;

    @ApiModelProperty(value = "用户uid")
    @Excel(name = "用户uid")
    private Long uid;

    @ApiModelProperty(value = "标题")
    @Excel(name = "标题")
    private String title;

    @ApiModelProperty(value = "是否公开（0不公开 1公开）")
    @Excel(name = "是否公开", readConverterExp = "0=不公开,1=公开")
    private Integer pub;
    private String pubText;

    @ApiModelProperty(value = "内容")
    @Excel(name = "内容")
    private String content;

    @ApiModelProperty(value = "图片")
    @Excel(name = "图片")
    private String urls;
    private List<String> pic;

    @ApiModelProperty(value = "发布时间")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date publishTime;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "浏览数")
    private Integer browseCount;

    @ApiModelProperty(value = "点赞数")
    private Integer likeCount;

    @ApiModelProperty(value = "评论数")
    private Integer commentCount;

    public void setPub(Integer pub) {
        this.pub = pub;
        if (pub == null) {
            return;
        }
        if (this.pub == 0) {
            this.pubText = "不公开";
        }
        if (this.pub == 1) {
            this.pubText = "公开";
        }
    }
}
