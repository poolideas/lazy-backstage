package com.lazy.system.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 预约对象 lazy_reserve
 * 
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_reserve")
public class Reserve extends Entity implements Serializable {

private static final long serialVersionUID=1L;
    /** 用户唯一标识uid */
    private Long uid;
    /** 教练唯一标识uid */
    private Long teacherUid;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 服务时间 */
    private Date serviceTime;
    /** 状态（1申请中 2已接受 3已拒绝 4已完成） */
    private Integer status;
    /** 预约要求 */
    private String demand;
}
