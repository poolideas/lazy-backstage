package com.lazy.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 评论对象 lazy_comment
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "CommentAddDto 评论添加修改视图模型")
public class CommentAddDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "被评论的主题ID")
    private Long topicId;
    @ApiModelProperty(value = "主题的类型（1课程 2文章 3公开课 4训练营 5觉醒日记）")
    private Integer topicType;
    @ApiModelProperty(value = "评论的内容")
    private String content;

}
