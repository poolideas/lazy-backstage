package com.lazy.system.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 点赞对象 lazy_like_record
 * 
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_like_record")
public class LikeRecord extends Entity implements Serializable {

private static final long serialVersionUID=1L;
    /** 用户唯一标识uid */
    private Long uid;
    /** 被点赞的主题ID */
    private Long topicId;
    /** 主题的类型（1课程 2文章 3公开课 4训练营） */
    private Integer topicType;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 点赞时间 */
    private Date likeTime;
}
