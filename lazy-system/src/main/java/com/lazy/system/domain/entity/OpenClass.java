package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 公开课对象 lazy_open_class
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_open_class")
public class OpenClass extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 教练uid
     */
    private Long uid;
    /**
     * 标题
     */
    private String title;
    /**
     * 副标题
     */
    private String subTitle;
    /**
     * 介绍
     */
    private String content;
    /**
     * 封面
     */
    private String pic;
    /**
     * 发布状态（0未发布 1已发布）
     */
    private Integer publishStatus;
}
