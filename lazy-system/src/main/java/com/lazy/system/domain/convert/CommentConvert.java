package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.CommentAddDto;
import com.lazy.system.domain.entity.Comment;
import com.lazy.system.domain.vo.CommentVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 评论类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface CommentConvert {

    CommentConvert INSTANCE = Mappers.getMapper(CommentConvert.class);

    Page<CommentVo> convertPage(Page<Comment> page);

    Comment convertAddDto(CommentAddDto dto);

    List<CommentVo> convertList(List<Comment> dto);

    CommentVo convert(Comment dto);

}

