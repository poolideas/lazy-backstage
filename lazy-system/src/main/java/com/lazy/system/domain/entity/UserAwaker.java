package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * 觉醒者用户信息对象 lazy_user_awaker
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_user_awaker")
public class UserAwaker extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 账号id
     */
    private Long uid;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 姓名
     */
    private String name;
    /**
     * 头像(相对路径)
     */
    private String avatar;
    /**
     * 性别（0女 1男 2未知）
     */
    private Integer gender;
    /**
     * 角色（0:普通用户 1:vip）
     */
    private Integer vip;
    /**
     * gallup标签
     */
    private String tags;
    /**
     * 目前从事的工作
     */
    private String job;
    /**
     * 期望从事的工作
     */
    private String wishJob;
}
