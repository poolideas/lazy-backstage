package com.lazy.system.domain.vo;

import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * gallup标签Vo对象 lazy_gallup_tag
 *
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "GallupTagVo gallup标签视图模型")
public class GallupTagVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private Long id;

    @ApiModelProperty(value = "编号1-34")
    @Excel(name = "编号1-34")
    private Long code;

    @ApiModelProperty(value = "标签分类（1执行力 2影响力 3建立关系 4战略思维）")
    @Excel(name = "标签分类", readConverterExp = "1=执行力,2=影响力,3=建立关系,4=战略思维")
    private Integer type;
    private String typeText;

    @ApiModelProperty(value = "标签分类颜色（1紫色 2橙色 3蓝色 4绿色）")
    @Excel(name = "标签分类颜色", readConverterExp = "1=紫色,2=橙色,3=蓝色,4=绿色")
    private Integer color;
    private String colorText;

    @ApiModelProperty(value = "名称")
    @Excel(name = "名称")
    private String name;

    @ApiModelProperty(value = "怎么说")
    @Excel(name = "怎么说")
    private String how;

    @ApiModelProperty(value = "这样说")
    @Excel(name = "这样说")
    private String saySo;

    @ApiModelProperty(value = "力量和优势")
    @Excel(name = "力量和优势")
    private String power;

    @ApiModelProperty(value = "如果是你的突出才干")
    @Excel(name = "如果是你的突出才干")
    private String seniorTalent;

    @ApiModelProperty(value = "如果是你的弱势才干")
    @Excel(name = "如果是你的弱势才干")
    private String lowerTalent;


    public void setType(Integer type) {
        this.type = type;
        if (type == null) {
            return;
        }
        if (this.type == 1) {
            this.typeText = "执行力" ;
        }
        if (this.type == 2) {
            this.typeText = "影响力" ;
        }
        if (this.type == 3) {
            this.typeText = "建立关系" ;
        }
        if (this.type == 4) {
            this.typeText = "战略思维" ;
        }
    }

    public void setColor(Integer color) {
        this.color = color;
        if (color == null) {
            return;
        }
        if (this.color == 1) {
            this.colorText = "紫色" ;
        }
        if (this.color == 2) {
            this.colorText = "橙色" ;
        }
        if (this.color == 3) {
            this.colorText = "蓝色" ;
        }
        if (this.color == 4) {
            this.colorText = "绿色" ;
        }
    }
}
