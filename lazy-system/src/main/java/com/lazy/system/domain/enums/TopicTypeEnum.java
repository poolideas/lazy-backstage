package com.lazy.system.domain.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created by Administrator on 2020/5/20.
 */
@Getter
public enum TopicTypeEnum {

    CLASS(1, "课程"),
    ARTICLE(2, "文章"),
    OPEN_CLASS(3, "公开课"),
    TRAIN_CAMP(4, "训练营"),
    AWAKE_DIARY(5, "觉醒日记");

    private Integer code;

    private String msg;

    TopicTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static String matchCode(Integer code) {
        return Arrays.stream(TopicTypeEnum.values())
                .filter(e -> e.getCode().equals(code))
                .map(TopicTypeEnum::getMsg)
                .findFirst().orElse(null);
    }


    public static Integer matchMsg(String msg) {
        return Arrays.stream(TopicTypeEnum.values())
                .filter(e -> e.getMsg().equals(msg))
                .map(TopicTypeEnum::getCode)
                .findFirst().orElse(null);
    }
}
