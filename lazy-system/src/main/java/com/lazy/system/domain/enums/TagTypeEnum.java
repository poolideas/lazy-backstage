package com.lazy.system.domain.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created by Administrator on 2020/5/20.
 */
@Getter
public enum TagTypeEnum {
    EXECUTIVE(1, "执行力"),
    EFFECT(2, "影响力"),
    relationship(3, "建立关系"),
    STRATEGY(4, "战略思维");

    private Integer code;

    private String msg;

    TagTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static String matchCode(Integer code) {
        return Arrays.stream(TagTypeEnum.values())
                .filter(e -> e.getCode().equals(code))
                .map(TagTypeEnum::getMsg)
                .findFirst().orElse(null);
    }


    public static Integer matchMsg(String msg) {
        return Arrays.stream(TagTypeEnum.values())
                .filter(e -> e.getMsg().equals(msg))
                .map(TagTypeEnum::getCode)
                .findFirst().orElse(null);
    }
}
