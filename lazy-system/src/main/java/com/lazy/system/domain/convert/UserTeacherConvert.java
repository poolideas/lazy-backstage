package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.UserTeacherAddDto;
import com.lazy.system.domain.entity.UserTeacher;
import com.lazy.system.domain.vo.UserTeacherVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 教练类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface UserTeacherConvert {

    UserTeacherConvert INSTANCE = Mappers.getMapper(UserTeacherConvert.class);

    Page<UserTeacherVo> convertPage(Page<UserTeacher> page);
    UserTeacher convertAddDto(UserTeacherAddDto dto);
    List<UserTeacherVo> convertList(List<UserTeacher> dto);
    UserTeacherVo convert(UserTeacher dto);

}

