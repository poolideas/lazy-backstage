package com.lazy.system.domain.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 觉醒日记Vo对象 lazy_awake_diary
 *
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AwakeDiaryQuery 觉醒日记查询视图模型")
public class AwakeDiaryQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "开始时间 yyyy-MM-dd")
    private Date beginTime;

    @ApiModelProperty(value = "结束时间 yyyy-MM-dd")
    private Date endTime;


}
