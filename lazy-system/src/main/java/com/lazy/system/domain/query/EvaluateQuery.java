package com.lazy.system.domain.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 预约评价Vo对象 lazy_evaluate
 * 
 * @author yang
 * @date 2021-02-09
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "EvaluateQuery 预约评价查询视图模型")
public class EvaluateQuery implements Serializable {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户id")
        private Long id;
    
    @ApiModelProperty(value = "用户唯一标识uid")
        private Long uid;
    
    @ApiModelProperty(value = "教练唯一标识uid")
        private Long teacherUid;
    
    @ApiModelProperty(value = "预约id")
        private Long reserveId;
    
    @ApiModelProperty(value = "评价时间")
        @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date evaluateTimeStart;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date evaluateTimeEnd;
    
    @ApiModelProperty(value = "评价分数")
        private Long evaluateScore;
    
    @ApiModelProperty(value = "评价内容")
        private String content;
    


}
