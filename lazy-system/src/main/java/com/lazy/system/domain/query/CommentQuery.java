package com.lazy.system.domain.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 评论Vo对象 lazy_comment
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "CommentQuery 评论查询视图模型")
public class CommentQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "被评论的主题ID")
    private Long topicId;

    @ApiModelProperty(value = "主题的类型（1课程 2文章 3公开课 4训练营 5觉醒日记）")
    private Integer topicType;

}
