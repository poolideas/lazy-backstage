package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 评论Vo对象 lazy_comment
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "CommentVo 评论视图模型")
public class CommentVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @Excel(name = "${comment}", readConverterExp = "id")
    private Long id;
    private String idText;

    @ApiModelProperty(value = "被评论的主题ID")
    @Excel(name = "被评论的主题ID")
    private Long topicId;

    @ApiModelProperty(value = "主题的类型（1课程 2文章 3公开课 4训练营）")
    @Excel(name = "主题的类型", readConverterExp = "1=课程,2=文章,3=公开课,4=训练营,5=觉醒日记")
    private Integer topicType;
    private String topicTypeText;

    @ApiModelProperty(value = "评论的内容")
    @Excel(name = "评论的内容")
    private String content;

    @ApiModelProperty(value = "评论者UID")
    private Long fromUid;

    @ApiModelProperty(value = "评论者昵称")
    private String fromName;

    @ApiModelProperty(value = "评论时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date commentTime;

    @ApiModelProperty(value = "回复评论的列表")
    private List<CommentReplyVo> list;



    public void setTopicType(Integer topicType) {
        this.topicType = topicType;
        if (topicType == null) {
            return;
        }
        if (this.topicType == 1) {
            this.topicTypeText = "课程" ;
        }
        if (this.topicType == 2) {
            this.topicTypeText = "文章" ;
        }
        if (this.topicType == 3) {
            this.topicTypeText = "公开课" ;
        }
        if (this.topicType == 4) {
            this.topicTypeText = "训练营" ;
        }
        if (this.topicType == 5) {
            this.topicTypeText = "觉醒日记" ;
        }
    }
}
