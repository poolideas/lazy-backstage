package com.lazy.system.domain.convert;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lazy.system.domain.dto.ArticleAddDto;
import com.lazy.system.domain.entity.Article;
import com.lazy.system.domain.vo.ArticleVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 文章类型转换
 *
 * @author yang
 * @date 2021-01-02
 */
@Mapper
public interface ArticleConvert {

    ArticleConvert INSTANCE = Mappers.getMapper(ArticleConvert.class);

    Page<ArticleVo> convertPage(Page<Article> page);

    Article convertAddDto(ArticleAddDto dto);

    List<ArticleVo> convertList(List<Article> dto);

    ArticleVo convert(Article dto);

}

