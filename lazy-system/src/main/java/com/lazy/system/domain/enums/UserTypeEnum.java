package com.lazy.system.domain.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * Created by Administrator on 2020/5/20.
 */
@Getter
public enum UserTypeEnum {
    AWAKE(1, "觉醒者"),
    TEACHER(2, "教练"),;

    private Integer code;

    private String msg;

    UserTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static String matchCode(Integer code) {
        return Arrays.stream(UserTypeEnum.values())
                .filter(e -> e.getCode().equals(code))
                .map(UserTypeEnum::getMsg)
                .findFirst().orElse(null);
    }


    public static Integer matchMsg(String msg) {
        return Arrays.stream(UserTypeEnum.values())
                .filter(e -> e.getMsg().equals(msg))
                .map(UserTypeEnum::getCode)
                .findFirst().orElse(null);
    }
}
