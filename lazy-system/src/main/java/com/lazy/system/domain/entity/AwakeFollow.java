package com.lazy.system.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 我的关注的人对象 lazy_awake_follow
 * 
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_awake_follow")
public class AwakeFollow extends Entity implements Serializable {

private static final long serialVersionUID=1L;
    /** 用户唯一标识uid */
    private Long uid;
    /** 我关注的人的uid */
    private Long followUid;
    /** 关注的人类型（1觉醒者 2教练） */
    private Integer type;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 关注时间 */
    private Date followTime;
}
