package com.lazy.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 公开课对象 lazy_open_class
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "OpenClassAddDto 公开课添加修改视图模型")
public class OpenClassAddDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "教练uid")
    private Long uid;
    @ApiModelProperty(value = "标题")
    private String title;
    @ApiModelProperty(value = "副标题")
    private String subTitle;
    @ApiModelProperty(value = "介绍")
    private String content;
    @ApiModelProperty(value = "封面")
    private String pic;
    @ApiModelProperty(value = "发布状态（0未发布 1已发布）")
    private Integer publishStatus;
}
