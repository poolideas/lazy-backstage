package com.lazy.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lazy.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 觉醒日记Vo对象 lazy_awake_diary
 *
 * @author yang
 * @date 2021-01-23
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AwakeDiaryVo 觉醒日记视图模型")
public class AwakeDiaryVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @Excel(name = "${comment}", readConverterExp = "用户id")
    private Long id;

    @ApiModelProperty(value = "用户唯一标识uid")
    @Excel(name = "用户唯一标识uid")
    private Long uid;

    @ApiModelProperty(value = "昵称")
    private String name;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "标题")
    @Excel(name = "标题")
    private String title;

    private String tags;

    @ApiModelProperty(value = "才干")
    private List<String> tagNames;

    @ApiModelProperty(value = "内容")
    @Excel(name = "内容")
    private String content;

    @ApiModelProperty(value = "是否公开（0不公开 1公开）")
    @Excel(name = "是否公开", readConverterExp = "0=不公开,1=公开")
    private Integer pub;
    private String pubText;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


    @ApiModelProperty(value = "浏览数")
    private Integer browseCount;

    @ApiModelProperty(value = "点赞数")
    private Integer likeCount;

    @ApiModelProperty(value = "评论数")
    private Integer commentCount;


    public void setPub(Integer pub) {
        this.pub = pub;
        if (pub == null) {
            return;
        }
        if (this.pub == 0) {
            this.pubText = "不公开" ;
        }
        if (this.pub == 1) {
            this.pubText = "公开" ;
        }
    }
}
