package com.lazy.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * gallup标签对象 lazy_gallup_tag
 *
 * @author yang
 * @date 2021-01-02
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "GallupTagAddDto gallup标签添加修改视图模型")
public class GallupTagDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "编号1-34")
    private Integer code;
    @ApiModelProperty(value = "标签分类（1执行力 2影响力 3建立关系 4战略思维）")
    private Integer type;
    @ApiModelProperty(value = "标签分类颜色（1紫色 2橙色 3蓝色 4绿色）")
    private Integer color;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "排序")
    private Integer sort;

}
