package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 课时对象 lazy_class
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_class")
public class LazyClass extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 训练营id/公开课id
     */
    private Long bizId;
    /**
     * 课时
     */
    private Long classPeriod;
    /**
     * 类型（1训练营 2公开课）
     */
    private Integer type;
    /**
     * 课程类型（1视频 2音频 3文章）
     */
    private Integer classType;
    /**
     * 副标题
     */
    private String subTitle;
    /**
     * 文章内容
     */
    private String content;
    /**
     * 视频地址
     */
    private String videoUrl;
    /**
     * 音频地址
     */
    private String audioUrl;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 开始时间 */
    private Date beginTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 结束时间 */
    private Date endTime;
    /**
     * 发布状态（0未发布 1已发布）
     */
    private Integer publishStatus;
}
