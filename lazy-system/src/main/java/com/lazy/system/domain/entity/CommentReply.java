package com.lazy.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


/**
 * 评论回复对象 lazy_comment_reply
 *
 * @author yang
 * @date 2021-01-21
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("lazy_comment_reply")
public class CommentReply extends Entity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 被评论的主题ID
     */
    private Long topicId;
    /**
     * 主题的类型（1课程 2文章 3公开课 4训练营）
     */
    private Integer topicType;
    /**
     * 被回复的评论的ID
     */
    private Long commentId;
    /**
     * 回复的内容
     */
    private String content;
    /**
     * 进行回复动作的用户的UID
     */
    private Long fromUid;
    /**
     * 被回复的用户UID
     */
    private Long toUid;
    /**
     * 如果reply_type=1，就是comment_id; 如果reply_type=2，就是被回复的回复ID
     */
    private Long replyId;
    /**
     * 回复类型（1评论 2回复）
     */
    private Integer replyType;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 评论时间 */
    private Date commentTime;
}
