package com.lazy.system.mapper;

import com.lazy.system.domain.entity.AwakeDiary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 觉醒日记Mapper接口
 *
 * @author yang
 * @date 2021-01-23
 */
public interface AwakeDiaryMapper extends BaseMapper<AwakeDiary> {

}
