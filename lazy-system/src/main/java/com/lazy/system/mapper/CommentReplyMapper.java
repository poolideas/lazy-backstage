package com.lazy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lazy.system.domain.entity.CommentReply;

/**
 * 评论回复Mapper接口
 *
 * @author yang
 * @date 2021-01-21
 */
public interface CommentReplyMapper extends BaseMapper<CommentReply> {

}
