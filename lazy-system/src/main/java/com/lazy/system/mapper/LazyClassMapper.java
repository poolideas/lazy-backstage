package com.lazy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lazy.system.domain.entity.LazyClass;

/**
 * 课时Mapper接口
 *
 * @author yang
 * @date 2021-01-21
 */
public interface LazyClassMapper extends BaseMapper<LazyClass> {

}
