package com.lazy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lazy.system.domain.entity.Comment;

/**
 * 评论Mapper接口
 *
 * @author yang
 * @date 2021-01-21
 */
public interface CommentMapper extends BaseMapper<Comment> {

}
