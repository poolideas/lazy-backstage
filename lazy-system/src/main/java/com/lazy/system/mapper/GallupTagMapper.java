package com.lazy.system.mapper;

import com.lazy.system.domain.entity.GallupTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * gallup标签Mapper接口
 *
 * @author yang
 * @date 2021-01-23
 */
public interface GallupTagMapper extends BaseMapper<GallupTag> {

}
