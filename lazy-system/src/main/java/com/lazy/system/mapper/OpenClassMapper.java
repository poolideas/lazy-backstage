package com.lazy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lazy.system.domain.entity.OpenClass;

/**
 * 公开课Mapper接口
 *
 * @author yang
 * @date 2021-01-21
 */
public interface OpenClassMapper extends BaseMapper<OpenClass> {

}
