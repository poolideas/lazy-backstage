package com.lazy.system.mapper;

import com.lazy.system.domain.entity.LikeRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 点赞Mapper接口
 *
 * @author yang
 * @date 2021-02-09
 */
public interface LikeRecordMapper extends BaseMapper<LikeRecord> {

}
