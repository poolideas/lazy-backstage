package com.lazy.system.mapper;

import com.lazy.system.domain.entity.Evaluate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 预约评价Mapper接口
 *
 * @author yang
 * @date 2021-02-09
 */
public interface EvaluateMapper extends BaseMapper<Evaluate> {

}
