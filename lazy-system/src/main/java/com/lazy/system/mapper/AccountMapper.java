package com.lazy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lazy.system.domain.entity.Account;

/**
 * 账户Mapper接口
 *
 * @author yang
 * @date 2021-01-02
 */
public interface AccountMapper extends BaseMapper<Account> {

}
