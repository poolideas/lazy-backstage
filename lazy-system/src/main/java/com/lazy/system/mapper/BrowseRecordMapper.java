package com.lazy.system.mapper;

import com.lazy.system.domain.entity.BrowseRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 浏览Mapper接口
 *
 * @author yang
 * @date 2021-02-09
 */
public interface BrowseRecordMapper extends BaseMapper<BrowseRecord> {

}
