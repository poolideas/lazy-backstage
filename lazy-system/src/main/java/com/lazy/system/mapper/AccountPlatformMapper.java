package com.lazy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lazy.system.domain.entity.AccountPlatform;

/**
 * 第三方用户信息Mapper接口
 *
 * @author yang
 * @date 2021-01-02
 */
public interface AccountPlatformMapper extends BaseMapper<AccountPlatform> {

}
