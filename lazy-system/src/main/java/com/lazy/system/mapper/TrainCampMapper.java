package com.lazy.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lazy.system.domain.entity.TrainCamp;

/**
 * 训练营Mapper接口
 *
 * @author yang
 * @date 2021-01-21
 */
public interface TrainCampMapper extends BaseMapper<TrainCamp> {

}
