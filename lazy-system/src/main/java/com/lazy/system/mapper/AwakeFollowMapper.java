package com.lazy.system.mapper;

import com.lazy.system.domain.entity.AwakeFollow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 我的关注的人Mapper接口
 *
 * @author yang
 * @date 2021-01-23
 */
public interface AwakeFollowMapper extends BaseMapper<AwakeFollow> {

}
