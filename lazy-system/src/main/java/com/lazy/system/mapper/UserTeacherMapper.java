package com.lazy.system.mapper;

import com.lazy.system.domain.entity.UserTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 教练Mapper接口
 *
 * @author yang
 * @date 2021-01-23
 */
public interface UserTeacherMapper extends BaseMapper<UserTeacher> {

}
