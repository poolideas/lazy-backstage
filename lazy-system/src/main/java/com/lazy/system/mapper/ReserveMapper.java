package com.lazy.system.mapper;

import com.lazy.system.domain.entity.Reserve;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 预约Mapper接口
 *
 * @author yang
 * @date 2021-02-09
 */
public interface ReserveMapper extends BaseMapper<Reserve> {

}
