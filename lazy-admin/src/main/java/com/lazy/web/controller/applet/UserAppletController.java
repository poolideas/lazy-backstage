package com.lazy.web.controller.applet;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.WxMaUserService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.core.domain.R;
import com.lazy.common.core.redis.RedisCache;
import com.lazy.common.exception.CustomException;
import com.lazy.common.utils.StringUtils;
import com.lazy.common.utils.TagUtil;
import com.lazy.common.utils.uuid.UUID;
import com.lazy.system.domain.convert.GallupTagConvert;
import com.lazy.system.domain.convert.UserAwakerConvert;
import com.lazy.system.domain.convert.UserTeacherConvert;
import com.lazy.system.domain.dto.AccountDto;
import com.lazy.system.domain.dto.UserAwakerAddDto;
import com.lazy.system.domain.dto.UserTeacherAddDto;
import com.lazy.system.domain.entity.Account;
import com.lazy.system.domain.entity.GallupTag;
import com.lazy.system.domain.entity.UserAwaker;
import com.lazy.system.domain.entity.UserTeacher;
import com.lazy.system.domain.enums.AppletLoginInitStepEnum;
import com.lazy.system.domain.enums.UserTypeEnum;
import com.lazy.system.domain.query.applet.AppletGetPhoneOrLoginDto;
import com.lazy.system.domain.query.applet.AppletUserRegisterAndLoginParam;
import com.lazy.system.domain.query.applet.TagQuery;
import com.lazy.system.domain.vo.*;
import com.lazy.system.service.IAccountService;
import com.lazy.system.service.IGallupTagService;
import com.lazy.system.service.IUserAwakerService;
import com.lazy.system.service.IUserTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 小程序登陆
 *
 * @author yang
 */
@RestController
@RequestMapping("/applet/user")
@Api(tags = "小程序用户相关服务", description = "UserAppletController")
public class UserAppletController extends BaseAppletController {

    @Autowired
    private WxMaService wxMaService;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IGallupTagService gallupTagService;
    @Autowired
    private IUserAwakerService userAwakerService;
    @Autowired
    private IUserTeacherService userTeacherService;
    @Autowired
    private RedisCache redisCache;

    @GetMapping("/openid/{code}")
    @ApiOperation("小程序获取openid")
    public R<String> getOpenid(@PathVariable String code) {
        WxMaJscode2SessionResult session = null;
        try {
            session = wxMaService.getUserService().getSessionInfo(code);
            redisCache.setCacheObject(session.getOpenid(), session.getSessionKey(), 1, TimeUnit.HOURS);
        } catch (WxErrorException e) {
            throw new RuntimeException(e.getError().getErrorCode() + e.getError().getErrorMsg(), e);
        }
        return new R<>(session.getOpenid());
    }

    @PostMapping("/getAppletPhoneOrLogin")
    @ApiOperation("小程序获取手机号或登陆")
    public R getAppletPhoneOrLogin(@RequestBody AppletGetPhoneOrLoginDto dto) {
        WxMaUserService userService = wxMaService.getUserService();
        String sessionKey = redisCache.getCacheObject(dto.getOpenid());
        WxMaPhoneNumberInfo phoneNoInfo = userService.getPhoneNoInfo(sessionKey, dto.getEncryptedData(), dto.getIv());
        Account account = accountService.getOne(Wrappers.<Account>lambdaQuery().eq(Account::getPhone, phoneNoInfo.getPhoneNumber()));
        if (account == null) {
            UserVo userVo = new UserVo();
            userVo.setPhone(phoneNoInfo.getPhoneNumber());
            return R.ok(userVo);
        } else {
            String token = UUID.fastUUID().toString();
            AccountDto accountDto = AccountDto.builder().uid(account.getId()).type(account.getType()).build();
            redisCache.setCacheObject(token, JSON.toJSONString(accountDto), 7, TimeUnit.DAYS);
            UserVo userVo = buildUserVo(account.getId(), dto.getNum(), dto.getSort());
            userVo.setToken(token);
            return R.ok(userVo);
        }
//        Account account = accountService.register(phoneNumber, dto.getOpenid(), dto.getType());
    }

    @GetMapping("/simulate/login/{uid}/{type}")
    @ApiOperation("模拟登陆方便自测")
    public R<String> simulateLogin(@PathVariable Long uid, @PathVariable Integer type) {
        String token = UUID.fastUUID().toString();
        AccountDto accountDto = AccountDto.builder().uid(uid).type(type).build();
        redisCache.setCacheObject(token, JSON.toJSONString(accountDto), 7, TimeUnit.DAYS);
        return new R<>(token);
    }

    @PostMapping("/registerOrLogin")
    @ApiOperation("小程序注册或登陆")
    public R<UserVo> registerOrLogin(@RequestBody AppletUserRegisterAndLoginParam dto) {
        if (dto.getType() == null || (dto.getType() != 1 && dto.getType() != 2)) {
            throw new CustomException("参数异常，请传入正确的用户类型");
        }
        Long uid = accountService.registerOrLogin(dto);
        String token = UUID.fastUUID().toString();
        AccountDto accountDto = AccountDto.builder().uid(uid).type(dto.getType()).build();
        redisCache.setCacheObject(token, JSON.toJSONString(accountDto), 7, TimeUnit.DAYS);
        UserVo userVo = buildUserVo(uid, dto.getNum(), dto.getSort());
        userVo.setToken(token);
        return new R<>(userVo);
    }

    @GetMapping("/detail")
    @ApiOperation("根据token获取用户信息")
    public R<UserVo> getUserInfo(TagQuery tagQuery) {
        return new R<>(buildUserVo(getAccountDto().getUid(), tagQuery.getNum(), tagQuery.getSort()));
    }

    @GetMapping("/tags")
    @ApiOperation("根据token获取标签列表")
    public R<List<GallupTagVo>> getTags(TagQuery tagQuery) {
        UserAwaker userAwaker = userAwakerService.getUserByUid(getAccountDto().getUid());
        if (userAwaker == null) {
            throw new CustomException("参数异常，请联系管理员！");
        }
        String tagStr = userAwaker.getTags();
        List<GallupTag> list = gallupTagService.getTagListByTagStr(tagStr, tagQuery.getNum(), tagQuery.getSort());
        return new R<>(GallupTagConvert.INSTANCE.convertList(list));
    }

    @PostMapping("/saveTags")
    @ApiOperation("根据token 保存标签")
    public R<Boolean> getTags(@RequestParam(value = "tags") String tags) {
        UserAwaker userAwaker = userAwakerService.getUserByUid(getAccountDto().getUid());
        if (userAwaker == null) {
            throw new CustomException("参数异常，请联系管理员！");
        }
        String[] tagArray = tags.split(",");
        String tagStr = TagUtil.getTagStrByList(Arrays.asList(tagArray));
        UserAwaker entity = new UserAwaker();
        entity.setId(userAwaker.getId());
        entity.setTags(tagStr);
        return new R<>(userAwakerService.updateById(entity));
    }

    @PostMapping("/save")
    @ApiOperation("根据token 保存觉醒者信息")
    public R<Boolean> save(@RequestBody UserAwakerAddDto dto) {
        UserAwaker userAwaker = userAwakerService.getUserByUid(getAccountDto().getUid());
        UserAwaker entity = UserAwakerConvert.INSTANCE.convertAddDto(dto);
        entity.setId(userAwaker.getId());
        entity.setUid(userAwaker.getUid());
        return new R<>(userAwakerService.updateById(entity));
    }

    @PostMapping("/saveTeacher")
    @ApiOperation("根据token 保存教练信息")
    public R<Boolean> saveTeacher(@RequestBody UserTeacherAddDto dto) {
        dto.setUid(getAccountDto().getUid());
        Boolean result = userTeacherService.saveTeacher(dto);
        return new R<>(result);
    }

    @PostMapping("/saveJob")
    @ApiOperation("根据token 保存期望工作")
    public R<Boolean> saveWishJobs(@RequestBody UserAwakerAddDto dto) {
        UserAwaker userAwaker = userAwakerService.getUserByUid(getAccountDto().getUid());
        UserAwaker entity = new UserAwaker();
        entity.setId(userAwaker.getId());
        entity.setJob(dto.getJob());
        entity.setWishJob(dto.getWishJob());
        return new R<>(userAwakerService.updateById(entity));
    }

    @GetMapping("/tagChart")
    @ApiOperation("根据token获取标签多维图")
    public R<List<GallupTagChartVo>> getTagsChart(TagQuery tagQuery) {
        UserAwaker userAwaker = userAwakerService.getUserByUid(getAccountDto().getUid());
        String tagStr = userAwaker.getTags();
        List<GallupTagChartVo> result = gallupTagService.getTagChartByTagStr(tagStr, tagQuery);

        return new R<>(result);
    }

    private UserVo buildUserVo(Long uid, Integer tagNum, Integer tagSort) {
        UserVo userVo = new UserVo();
        Account account = accountService.getById(uid);
        userVo.setType(account.getType());
        userVo.setPhone(account.getPhone());
        if (UserTypeEnum.AWAKE.getCode().equals(account.getType())) {
            UserAwaker userAwaker = userAwakerService.getUserByUid(uid);
            UserAwakerVo userAwakerVo = UserAwakerConvert.INSTANCE.convert(userAwaker);
            userAwakerVo.setPhone(account.getPhone());
            List<GallupTag> list = gallupTagService.getTagListByTagStr(userAwakerVo.getTags(), tagNum, tagSort);
            List<GallupTagVo> gallupTagVos = GallupTagConvert.INSTANCE.convertList(list);
            Integer step = AppletLoginInitStepEnum.LOGIN.getCode();
            if (StringUtils.isEmpty(userAwaker.getTags())) {
                step = AppletLoginInitStepEnum.REGISTER2.getCode();
            } else if (StringUtils.isEmpty(userAwaker.getWishJob())) {
                step = AppletLoginInitStepEnum.REGISTER3.getCode();
            }
            userVo.setStatus(step);
            userVo.setTags(gallupTagVos);
            userVo.setUserAwakerVo(userAwakerVo);
        } else if (UserTypeEnum.TEACHER.getCode().equals(account.getType())) {
            UserTeacher userTeacher = userTeacherService.getUserByUid(uid);
            UserTeacherVo userTeacherVo = UserTeacherConvert.INSTANCE.convert(userTeacher);
            userTeacherVo.setPhone(account.getPhone());
            userVo.setUserTeacherVo(userTeacherVo);
        }
        return userVo;
    }


}
