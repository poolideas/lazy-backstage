package com.lazy.web.controller.applet;

import com.lazy.common.annotation.Log;
import com.lazy.common.core.domain.R;
import com.lazy.common.enums.BusinessType;
import com.lazy.system.domain.convert.BrowseRecordConvert;
import com.lazy.system.domain.dto.AccountDto;
import com.lazy.system.domain.dto.BrowseRecordAddDto;
import com.lazy.system.domain.entity.BrowseRecord;
import com.lazy.system.service.IBrowseRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 浏览Controller
 *
 * @author yang
 * @date 2021-02-09
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/applet/browse")
@Api(tags = "浏览相关服务", description = "BrowseRecordController")
public class BrowseRecordController extends BaseAppletController {

    private final IBrowseRecordService iBrowseRecordService;

    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "浏览信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("添加")
    public R add(@RequestBody BrowseRecordAddDto dto) {
        BrowseRecord entity = BrowseRecordConvert.INSTANCE.convertAddDto(dto);
        AccountDto accountDto = getAnonymousAccount();
        entity.setUid(accountDto.getUid());
        return new R<>(iBrowseRecordService.save(entity));
    }

}
