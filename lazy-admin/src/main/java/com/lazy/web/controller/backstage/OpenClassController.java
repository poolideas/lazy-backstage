package com.lazy.web.controller.backstage;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.annotation.Log;
import com.lazy.common.core.controller.BaseController;
import com.lazy.common.core.domain.AjaxResult;
import com.lazy.common.core.domain.R;
import com.lazy.common.core.page.TableDataInfo;
import com.lazy.common.enums.BusinessType;
import com.lazy.common.enums.SystemEnum;
import com.lazy.common.utils.ApiAssert;
import com.lazy.common.utils.StringUtils;
import com.lazy.common.utils.poi.ExcelUtil;
import com.lazy.system.domain.convert.OpenClassConvert;
import com.lazy.system.domain.dto.OpenClassAddDto;
import com.lazy.system.domain.entity.OpenClass;
import com.lazy.system.domain.query.OpenClassQuery;
import com.lazy.system.domain.vo.OpenClassVo;
import com.lazy.system.service.IOpenClassService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 公开课Controller
 *
 * @author yang
 * @date 2021-01-21
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/applet/openClass")
@Api(tags = "后台公开课相关服务", description = "OpenClassController")
public class OpenClassController extends BaseController {

    private final IOpenClassService iOpenClassService;

    /**
     * 查询公开课列表
     */
    @GetMapping("/list")
    public TableDataInfo list(OpenClassQuery dto) {
        startPage();
        LambdaQueryWrapper<OpenClass> lqw = Wrappers.lambdaQuery();
        lqw.eq(dto.getUid() != null, OpenClass::getUid, dto.getUid());
        lqw.like(StringUtils.isNotBlank(dto.getTitle()), OpenClass::getTitle, dto.getTitle());
        lqw.like(StringUtils.isNotBlank(dto.getSubTitle()), OpenClass::getSubTitle, dto.getSubTitle());
        lqw.eq(dto.getPublishStatus() != null, OpenClass::getPublishStatus, dto.getPublishStatus());
        List<OpenClass> list = iOpenClassService.list(lqw);
        return getDataTable(list);
    }

    /**
     * 导出公开课列表
     */
    @Log(title = "公开课", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation("导出")
    public AjaxResult export(OpenClassQuery dto) {
        LambdaQueryWrapper<OpenClass> lqw = new LambdaQueryWrapper<OpenClass>();
        List<OpenClass> list = iOpenClassService.list(lqw);
        ExcelUtil<OpenClass> util = new ExcelUtil<OpenClass>(OpenClass.class);
        return util.exportExcel(list, "openClass");
    }

    /**
     * 获取觉醒者用户信息详细信息
     */
    @GetMapping(value = "/{id}")
    @ApiOperation("详情")
    public R<OpenClassVo> getInfo(@PathVariable("id") Long id) {
        OpenClass entity = iOpenClassService.getById(id);
        return new R<>(OpenClassConvert.INSTANCE.convert(entity));
    }

    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "公开课信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("添加")
    public R add(@RequestBody OpenClassAddDto dto) {
        ApiAssert.isNull(SystemEnum.ID_NOT_NULL, dto.getId());
        return new R<>(iOpenClassService.save(OpenClassConvert.INSTANCE.convertAddDto(dto)));
    }

    /**
     * 修改觉醒者用户信息
     */
    @Log(title = "公开课信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ApiOperation("修改")
    public R edit(@RequestBody OpenClassAddDto dto) {
        ApiAssert.notNull(SystemEnum.ID_NULL, dto.getId());
        return new R<>(iOpenClassService.updateById(OpenClassConvert.INSTANCE.convertAddDto(dto)));
    }

    /**
     * 删除公开课信息
     */
    @Log(title = "公开课信息", businessType = BusinessType.DELETE)
    @GetMapping("/delete/{ids}")
    @ApiOperation("删除")
    public R remove(@PathVariable Long[] ids) {
        ApiAssert.notNull(SystemEnum.ID_NULL, ids);
        return new R<>(iOpenClassService.removeByIds(Arrays.asList(ids)));
    }
}
