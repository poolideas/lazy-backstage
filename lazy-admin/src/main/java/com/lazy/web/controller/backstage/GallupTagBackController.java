package com.lazy.web.controller.backstage;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.annotation.Log;
import com.lazy.common.core.controller.BaseController;
import com.lazy.common.core.domain.AjaxResult;
import com.lazy.common.core.domain.R;
import com.lazy.common.core.page.TableDataInfo;
import com.lazy.common.enums.BusinessType;
import com.lazy.common.enums.SystemEnum;
import com.lazy.common.utils.ApiAssert;
import com.lazy.common.utils.StringUtils;
import com.lazy.common.utils.poi.ExcelUtil;
import com.lazy.system.domain.convert.GallupTagConvert;
import com.lazy.system.domain.dto.GallupTagAddDto;
import com.lazy.system.domain.entity.GallupTag;
import com.lazy.system.domain.query.GallupTagQuery;
import com.lazy.system.domain.vo.GallupTagVo;
import com.lazy.system.service.IGallupTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * gallup标签Controller
 *
 * @author yang
 * @date 2021-01-02
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/gallup/tag")
@Api(tags = "后台gallup标签相关服务", description = "GallupTagController")
public class GallupTagBackController extends BaseController {

    private final IGallupTagService iGallupTagService;

    /**
     * 查询gallup标签列表
     */
    @GetMapping("/list")
    @ApiOperation("列表")
    @PreAuthorize("@ss.hasPermi('system:tag:list')")
    public TableDataInfo list(GallupTagQuery dto) {
        startPage();
        LambdaQueryWrapper<GallupTag> lqw = Wrappers.lambdaQuery();
        lqw.eq(dto.getCode() != null, GallupTag::getCode, dto.getCode());
        lqw.eq(dto.getType() != null, GallupTag::getType, dto.getType());
        lqw.eq(dto.getColor() != null, GallupTag::getColor, dto.getColor());
        lqw.like(StringUtils.isNotBlank(dto.getName()), GallupTag::getName, dto.getName());
        List<GallupTag> list = iGallupTagService.list(lqw);
        return getDataTable(list);
    }
    /**
     * 导出gallup标签列表
     */
    @Log(title = "gallup标签", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation("导出")
    @PreAuthorize("@ss.hasPermi('system:tag:export')")
    public AjaxResult export(GallupTagQuery dto) {
        LambdaQueryWrapper<GallupTag> lqw = new LambdaQueryWrapper<GallupTag>();
        List<GallupTag> list = iGallupTagService.list(lqw);
        ExcelUtil<GallupTag> util = new ExcelUtil<GallupTag>(GallupTag.class);
        return util.exportExcel(list, "tag");
    }

    /**
     * 获取觉醒者用户信息详细信息
     */
    @GetMapping(value = "/{id}")
    @ApiOperation("详情")
    @PreAuthorize("@ss.hasPermi('system:dept:query')")
    public R<GallupTagVo> getInfo(@PathVariable("id") Long id) {
        GallupTag entity = iGallupTagService.getById(id);
        return new R<>(GallupTagConvert.INSTANCE.convert(entity));
    }

    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "gallup标签信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("添加")
    @PreAuthorize("@ss.hasPermi('system:tag:add')")
    public R add(@RequestBody GallupTagAddDto dto) {
        ApiAssert.isNull(SystemEnum.ID_NOT_NULL, dto.getId());
        return new R<>(iGallupTagService.save(GallupTagConvert.INSTANCE.convertAddDto(dto)));
    }

    /**
     * 修改觉醒者用户信息
     */
    @Log(title = "gallup标签信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ApiOperation("修改")
    @PreAuthorize("@ss.hasPermi('system:tag:edit')")
    public R edit(@RequestBody GallupTagAddDto dto) {
        ApiAssert.notNull(SystemEnum.ID_NULL, dto.getId());
        return new R<>(iGallupTagService.updateById(GallupTagConvert.INSTANCE.convertAddDto(dto)));
    }

    /**
     * 删除gallup标签信息
     */
    @Log(title = "gallup标签信息", businessType = BusinessType.DELETE)
    @GetMapping("/delete/{ids}")
    @ApiOperation("删除")
    @PreAuthorize("@ss.hasPermi('system:tag:remove')")
    public R remove(@PathVariable Long[] ids) {
        ApiAssert.notNull(SystemEnum.ID_NULL, ids);
        return new R<>(iGallupTagService.removeByIds(Arrays.asList(ids)));
    }
}
