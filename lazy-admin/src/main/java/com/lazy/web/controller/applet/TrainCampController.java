//package com.lazy.web.controller.applet;
//
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.lazy.common.annotation.Log;
//import com.lazy.common.core.controller.BaseController;
//import com.lazy.common.core.domain.AjaxResult;
//import com.lazy.common.core.domain.R;
//import com.lazy.common.core.page.TableDataInfo;
//import com.lazy.common.enums.BusinessType;
//import com.lazy.common.enums.SystemEnum;
//import com.lazy.common.utils.ApiAssert;
//import com.lazy.common.utils.StringUtils;
//import com.lazy.common.utils.poi.ExcelUtil;
//import com.lazy.system.domain.convert.TrainCampConvert;
//import com.lazy.system.domain.dto.TrainCampAddDto;
//import com.lazy.system.domain.entity.TrainCamp;
//import com.lazy.system.domain.query.TrainCampQuery;
//import com.lazy.system.domain.vo.TrainCampVo;
//import com.lazy.system.service.ITrainCampService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.Arrays;
//import java.util.List;
//
///**
// * 训练营Controller
// *
// * @author yang
// * @date 2021-01-21
// */
//@RequiredArgsConstructor(onConstructor_ = @Autowired)
//@RestController
//@RequestMapping("/applet/camp")
//@Api(tags = "训练营相关服务", description = "TrainCampController")
//public class TrainCampController extends BaseController {
//
//    private final ITrainCampService iTrainCampService;
//
//    /**
//     * 查询训练营列表
//     */
//    @GetMapping("/list")
//    public TableDataInfo list(TrainCampQuery dto) {
//        startPage();
//        LambdaQueryWrapper<TrainCamp> lqw = Wrappers.lambdaQuery();
//        lqw.eq(dto.getUid() != null, TrainCamp::getUid, dto.getUid());
//        lqw.like(StringUtils.isNotBlank(dto.getTitle()), TrainCamp::getTitle, dto.getTitle());
//        lqw.like(StringUtils.isNotBlank(dto.getSubTitle()), TrainCamp::getSubTitle, dto.getSubTitle());
//        lqw.ge(dto.getBeginTimeStart() != null, TrainCamp::getBeginTime, dto.getBeginTimeStart());
//        lqw.lt(dto.getBeginTimeEnd() != null, TrainCamp::getBeginTime, dto.getBeginTimeEnd());
//        lqw.ge(dto.getEndTimeStart() != null, TrainCamp::getEndTime, dto.getEndTimeStart());
//        lqw.lt(dto.getEndTimeEnd() != null, TrainCamp::getEndTime, dto.getEndTimeEnd());
//        lqw.eq(dto.getPublishStatus() != null, TrainCamp::getPublishStatus, dto.getPublishStatus());
//        lqw.eq(dto.getDoingStatus() != null, TrainCamp::getDoingStatus, dto.getDoingStatus());
//        List<TrainCamp> list = iTrainCampService.list(lqw);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出训练营列表
//     */
//    @Log(title = "训练营", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    @ApiOperation("导出")
//    public AjaxResult export(TrainCampQuery dto) {
//        LambdaQueryWrapper<TrainCamp> lqw = new LambdaQueryWrapper<TrainCamp>();
//        List<TrainCamp> list = iTrainCampService.list(lqw);
//        ExcelUtil<TrainCamp> util = new ExcelUtil<TrainCamp>(TrainCamp.class);
//        return util.exportExcel(list, "camp");
//    }
//
//    /**
//     * 获取觉醒者用户信息详细信息
//     */
//    @GetMapping(value = "/{id}")
//    @ApiOperation("详情")
//    public R<TrainCampVo> getInfo(@PathVariable("id") Long id) {
//        TrainCamp entity = iTrainCampService.getById(id);
//        return new R<>(TrainCampConvert.INSTANCE.convert(entity));
//    }
//
//    /**
//     * 新增觉醒者用户信息
//     */
//    @Log(title = "训练营信息", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    @ApiOperation("添加")
//    public R add(@RequestBody TrainCampAddDto dto) {
//        ApiAssert.isNull(SystemEnum.ID_NOT_NULL, dto.getId());
//        return new R<>(iTrainCampService.save(TrainCampConvert.INSTANCE.convertAddDto(dto)));
//    }
//
//    /**
//     * 修改觉醒者用户信息
//     */
//    @Log(title = "训练营信息", businessType = BusinessType.UPDATE)
//    @PostMapping("/update")
//    @ApiOperation("修改")
//    public R edit(@RequestBody TrainCampAddDto dto) {
//        ApiAssert.notNull(SystemEnum.ID_NULL, dto.getId());
//        return new R<>(iTrainCampService.updateById(TrainCampConvert.INSTANCE.convertAddDto(dto)));
//    }
//
//    /**
//     * 删除训练营信息
//     */
//    @Log(title = "训练营信息", businessType = BusinessType.DELETE)
//    @GetMapping("/delete/{ids}")
//    @ApiOperation("删除")
//    public R remove(@PathVariable Long[] ids) {
//        ApiAssert.notNull(SystemEnum.ID_NULL, ids);
//        return new R<>(iTrainCampService.removeByIds(Arrays.asList(ids)));
//    }
//}
