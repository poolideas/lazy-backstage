//package com.lazy.web.controller.applet;
//
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.lazy.common.annotation.Log;
//import com.lazy.common.core.controller.BaseController;
//import com.lazy.common.core.domain.AjaxResult;
//import com.lazy.common.core.domain.R;
//import com.lazy.common.core.page.TableDataInfo;
//import com.lazy.common.enums.BusinessType;
//import com.lazy.common.enums.SystemEnum;
//import com.lazy.common.utils.ApiAssert;
//import com.lazy.common.utils.StringUtils;
//import com.lazy.common.utils.poi.ExcelUtil;
//import com.lazy.system.domain.convert.LazyClassConvert;
//import com.lazy.system.domain.dto.LazyClassAddDto;
//import com.lazy.system.domain.entity.LazyClass;
//import com.lazy.system.domain.query.LazyClassQuery;
//import com.lazy.system.domain.vo.LazyClassVo;
//import com.lazy.system.service.ILazyClassService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.Arrays;
//import java.util.List;
//
///**
// * 课时Controller
// *
// * @author yang
// * @date 2021-01-21
// */
//@RequiredArgsConstructor(onConstructor_ = @Autowired)
//@RestController
//@RequestMapping("/applet/class")
//@Api(tags = "课时相关服务", description = "LazyClassController")
//public class LazyClassController extends BaseController {
//
//    private final ILazyClassService iLazyClassService;
//
//    /**
//     * 查询课时列表
//     */
//    @GetMapping("/list")
//    public TableDataInfo list(LazyClassQuery dto) {
//        startPage();
//        LambdaQueryWrapper<LazyClass> lqw = Wrappers.lambdaQuery();
//        lqw.eq(dto.getBizId() != null, LazyClass::getBizId, dto.getBizId());
//        lqw.eq(dto.getClassPeriod() != null, LazyClass::getClassPeriod, dto.getClassPeriod());
//        lqw.eq(dto.getType() != null, LazyClass::getType, dto.getType());
//        lqw.eq(dto.getClassType() != null, LazyClass::getClassType, dto.getClassType());
//        lqw.like(StringUtils.isNotBlank(dto.getSubTitle()), LazyClass::getSubTitle, dto.getSubTitle());
//        lqw.eq(dto.getPublishStatus() != null, LazyClass::getPublishStatus, dto.getPublishStatus());
//        List<LazyClass> list = iLazyClassService.list(lqw);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出课时列表
//     */
//    @Log(title = "课时", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    @ApiOperation("导出")
//    public AjaxResult export(LazyClassQuery dto) {
//        LambdaQueryWrapper<LazyClass> lqw = new LambdaQueryWrapper<LazyClass>();
//        List<LazyClass> list = iLazyClassService.list(lqw);
//        ExcelUtil<LazyClass> util = new ExcelUtil<LazyClass>(LazyClass.class);
//        return util.exportExcel(list, "class");
//    }
//
//    /**
//     * 获取觉醒者用户信息详细信息
//     */
//    @GetMapping(value = "/{id}")
//    @ApiOperation("详情")
//    public R<LazyClassVo> getInfo(@PathVariable("id") Long id) {
//        LazyClass entity = iLazyClassService.getById(id);
//        return new R<>(LazyClassConvert.INSTANCE.convert(entity));
//    }
//
//    /**
//     * 新增觉醒者用户信息
//     */
//    @Log(title = "课时信息", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    @ApiOperation("添加")
//    public R add(@RequestBody LazyClassAddDto dto) {
//        ApiAssert.isNull(SystemEnum.ID_NOT_NULL, dto.getId());
//        return new R<>(iLazyClassService.save(LazyClassConvert.INSTANCE.convertAddDto(dto)));
//    }
//
//    /**
//     * 修改觉醒者用户信息
//     */
//    @Log(title = "课时信息", businessType = BusinessType.UPDATE)
//    @PostMapping("/update")
//    @ApiOperation("修改")
//    public R edit(@RequestBody LazyClassAddDto dto) {
//        ApiAssert.notNull(SystemEnum.ID_NULL, dto.getId());
//        return new R<>(iLazyClassService.updateById(LazyClassConvert.INSTANCE.convertAddDto(dto)));
//    }
//
//    /**
//     * 删除课时信息
//     */
//    @Log(title = "课时信息", businessType = BusinessType.DELETE)
//    @GetMapping("/delete/{ids}")
//    @ApiOperation("删除")
//    public R remove(@PathVariable Long[] ids) {
//        ApiAssert.notNull(SystemEnum.ID_NULL, ids);
//        return new R<>(iLazyClassService.removeByIds(Arrays.asList(ids)));
//    }
//}
