package com.lazy.web.controller.applet;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.annotation.Log;
import com.lazy.common.core.domain.R;
import com.lazy.common.enums.BusinessType;
import com.lazy.common.enums.SystemEnum;
import com.lazy.common.utils.ApiAssert;
import com.lazy.system.domain.convert.CommentConvert;
import com.lazy.system.domain.convert.CommentReplyConvert;
import com.lazy.system.domain.dto.CommentAddDto;
import com.lazy.system.domain.dto.CommentReplyAddDto;
import com.lazy.system.domain.entity.Comment;
import com.lazy.system.domain.entity.CommentReply;
import com.lazy.system.domain.query.CommentQuery;
import com.lazy.system.domain.vo.CommentReplyVo;
import com.lazy.system.domain.vo.CommentVo;
import com.lazy.system.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 评论Controller
 *
 * @author yang
 * @date 2021-01-21
 */
@RestController
@RequestMapping("/applet/comment")
@Api(tags = "小程序评论相关服务", description = "CommentController")
public class CommentController extends BaseAppletController {

    @Autowired
    private ICommentService commentService;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private ICommentReplyService commentReplyService;
    @Autowired
    private IUserAwakerService awakerService;
    @Autowired
    private IUserTeacherService teacherService;

    /**
     * 查询评论列表
     */
    @GetMapping("/list")
    @ApiOperation("查询评论列表")
    public R list(CommentQuery dto) {
        LambdaQueryWrapper<Comment> lqw = Wrappers.lambdaQuery();
        lqw.eq(Comment::getTopicId, dto.getTopicId());
        lqw.eq(Comment::getTopicType, dto.getTopicType());
        lqw.orderByDesc(Comment::getCommentTime);
        List<Comment> list = commentService.list(lqw);

        LambdaQueryWrapper<CommentReply> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(CommentReply::getTopicId, dto.getTopicId());
        wrapper.eq(CommentReply::getTopicType, dto.getTopicType());
        wrapper.orderByDesc(CommentReply::getCommentTime);
        List<CommentReply> commentReplyList = commentReplyService.list(wrapper);

        Map<Long, List<CommentReplyVo>> map = commentReplyList.stream().map(commentReply -> {
            CommentReplyVo vo = CommentReplyConvert.INSTANCE.convert(commentReply);
            vo.setFromName(accountService.getNameByUid(vo.getFromUid()));
            vo.setToName(accountService.getNameByUid(vo.getToUid()));
            return vo;
        }).collect(Collectors.groupingBy(CommentReplyVo::getCommentId));

        List<CommentVo> vos = list.stream().map(comment -> {
            CommentVo vo = CommentConvert.INSTANCE.convert(comment);
            vo.setFromName(accountService.getNameByUid(vo.getFromUid()));
            vo.setList(map.get(vo.getId()));
            return vo;
        }).collect(Collectors.toList());
        return new R<>(vos);
    }

    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "评论信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("添加评论")
    public R add(@RequestBody CommentAddDto dto) {
        ApiAssert.isNull(SystemEnum.ID_NOT_NULL, dto.getId());
        Comment entity = CommentConvert.INSTANCE.convertAddDto(dto);
        entity.setFromUid(getAccountDto().getUid());
        return new R<>(commentService.save(entity));
    }

    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "评论回复信息", businessType = BusinessType.INSERT)
    @PostMapping("/addReply")
    @ApiOperation("添加回复信息")
    public R addReply(@RequestBody CommentReplyAddDto dto) {
        Comment comment = commentService.getById(dto.getCommentId());
        CommentReply entity = CommentReplyConvert.INSTANCE.convertAddDto(dto);
        entity.setTopicId(comment.getTopicId());
        entity.setTopicType(comment.getTopicType());
        entity.setFromUid(getAccountDto().getUid());
        return new R<>(commentReplyService.save(entity));
    }


    /**
     * 删除评论信息
     */
    @Log(title = "评论信息", businessType = BusinessType.DELETE)
    @GetMapping("/delete/{ids}")
    @ApiOperation("删除评论（包括回复）")
    public R remove(@PathVariable String ids) {
        ApiAssert.notNull(SystemEnum.ID_NULL, ids);
        String[] idArrays = ids.split(",");
        return new R<>(commentService.deleteByIds(Arrays.asList(idArrays)));
    }

    /**
     * 删除回复信息
     */
    @Log(title = "评论信息", businessType = BusinessType.DELETE)
    @GetMapping("/reply/delete/{ids}")
    @ApiOperation("删除回复")
    public R replyRemove(@PathVariable String ids) {
        ApiAssert.notNull(SystemEnum.ID_NULL, ids);
        String[] idArrays = ids.split(",");
        return new R<>(commentReplyService.removeByIds(Arrays.asList(idArrays)));
    }

}
