package com.lazy.web.controller.applet;

import com.alibaba.fastjson.JSON;
import com.lazy.common.core.controller.BaseController;
import com.lazy.common.exception.BaseException;
import com.lazy.common.exception.CustomException;
import com.lazy.common.utils.ServletUtils;
import com.lazy.common.utils.StringUtils;
import com.lazy.system.domain.dto.AccountDto;
import com.lazy.system.domain.enums.UserTypeEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * web层通用数据处理
 *
 * @author yang
 */
@Slf4j
public class BaseAppletController extends BaseController {
    /**
     * 获取账户
     */
    protected AccountDto getAccountDto() {
        return getAccountDtoByToken();
    }

    /**
     * 获取账户 并且是觉醒者
     */
    protected AccountDto getAccountAndValidateAwaker() {
        AccountDto accountDto = getAccountDtoByToken();
        if (!UserTypeEnum.AWAKE.getCode().equals(accountDto.getType())) {
            throw new CustomException("账号不是觉醒者");
        }
        return accountDto;
    }

    /**
     * 获取账户 并且是教练
     */
    protected AccountDto getAccountAndValidateTeacher() {
        AccountDto accountDto = getAccountDtoByToken();
        if (!UserTypeEnum.TEACHER.getCode().equals(accountDto.getType())) {
            throw new CustomException("账号不是教练");
        }
        return accountDto;
    }

    private AccountDto getAccountDtoByToken() {
        String token = ServletUtils.getheaderParameterToLong("token");
        if (StringUtils.isEmpty(token)) {
            throw new BaseException("999999", "请登陆");
        }
        String accountDtoJson = redisCache.getCacheObject(token);
        if (StringUtils.isEmpty(accountDtoJson)) {
            throw new BaseException("999999", "请登陆");
        }
        AccountDto accountDto = JSON.parseObject(accountDtoJson, AccountDto.class);
        if (accountDto == null) {
            throw new BaseException("999999", "请登陆");
        }
        if (accountDto.getUid() == null) {
            throw new BaseException("999999", "请登陆");
        }
        return accountDto;
    }

    protected AccountDto getAnonymousAccount() {
        String token = ServletUtils.getheaderParameterToLong("token");
        if (StringUtils.isEmpty(token)) {
            return new AccountDto();
        }
        String accountDtoJson = redisCache.getCacheObject(token);
        if (StringUtils.isEmpty(accountDtoJson)) {
            return new AccountDto();
        }
        AccountDto accountDto = JSON.parseObject(accountDtoJson, AccountDto.class);
        if (accountDto == null) {
            return new AccountDto();
        }
        if (accountDto.getUid() == null) {
            return new AccountDto();
        }
        return accountDto;
    }
}
