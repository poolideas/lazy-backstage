package com.lazy.web.controller.applet;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.core.controller.BaseController;
import com.lazy.common.core.domain.R;
import com.lazy.common.core.page.TableDataInfo;
import com.lazy.common.utils.StringUtils;
import com.lazy.system.domain.convert.GallupTagConvert;
import com.lazy.system.domain.entity.GallupTag;
import com.lazy.system.domain.query.GallupTagQuery;
import com.lazy.system.domain.vo.GallupTagVo;
import com.lazy.system.service.IGallupTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * gallup标签Controller
 *
 * @author yang
 * @date 2021-01-02
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/applet/gallup/tag")
@Api(tags = "小程序gallup标签相关服务", description = "GallupTagController")
public class GallupTagController extends BaseController {

    private final IGallupTagService iGallupTagService;

    /**
     * 查询gallup标签列表
     */
    @GetMapping("/list")
    @ApiOperation("列表")
    public TableDataInfo list(GallupTagQuery dto) {
        startPage();
        LambdaQueryWrapper<GallupTag> lqw = Wrappers.lambdaQuery();
        lqw.eq(dto.getCode() != null, GallupTag::getCode, dto.getCode());
        lqw.eq(dto.getType() != null, GallupTag::getType, dto.getType());
        lqw.eq(dto.getColor() != null, GallupTag::getColor, dto.getColor());
        lqw.like(StringUtils.isNotBlank(dto.getName()), GallupTag::getName, dto.getName());
        List<GallupTag> list = iGallupTagService.list(lqw);
        return getDataTable(list);
    }

    /**
     * 获取觉醒者用户信息详细信息
     */
    @GetMapping(value = "/{id}")
    @ApiOperation("详情")
    public R<GallupTagVo> getInfo(@PathVariable("id") Long id) {
        GallupTag entity = iGallupTagService.getById(id);
        return new R<>(GallupTagConvert.INSTANCE.convert(entity));
    }
}
