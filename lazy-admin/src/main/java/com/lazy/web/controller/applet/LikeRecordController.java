package com.lazy.web.controller.applet;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.annotation.Log;
import com.lazy.common.core.domain.R;
import com.lazy.common.enums.BusinessType;
import com.lazy.system.domain.convert.LikeRecordConvert;
import com.lazy.system.domain.dto.AccountDto;
import com.lazy.system.domain.dto.LikeRecordAddDto;
import com.lazy.system.domain.entity.LikeRecord;
import com.lazy.system.service.ILikeRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 点赞Controller
 *
 * @author yang
 * @date 2021-02-09
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/applet/like")
@Api(tags = "点赞相关服务", description = "LikeRecordController")
public class LikeRecordController extends BaseAppletController {

    private final ILikeRecordService iLikeRecordService;

    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "点赞信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("添加")
    public R add(@RequestBody LikeRecordAddDto dto) {
        LikeRecord entity = LikeRecordConvert.INSTANCE.convertAddDto(dto);
        AccountDto accountDto = getAnonymousAccount();
        entity.setUid(accountDto.getUid());
        return new R<>(iLikeRecordService.save(entity));
    }


    /**
     * 删除点赞信息
     */
    @Log(title = "点赞信息", businessType = BusinessType.DELETE)
    @GetMapping("/cancle")
    @ApiOperation("取消")
    public R remove(@RequestBody LikeRecordAddDto dto) {
        LambdaQueryWrapper<LikeRecord> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(LikeRecord::getTopicId, dto.getTopicId());
        wrapper.eq(LikeRecord::getTopicType, dto.getTopicType());
        wrapper.eq(LikeRecord::getUid, getAccountDto().getUid());
        return new R<>(iLikeRecordService.remove(wrapper));
    }
}
