package com.lazy.web.controller.applet;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.annotation.Log;
import com.lazy.common.core.domain.AjaxResult;
import com.lazy.common.core.domain.R;
import com.lazy.common.core.page.TableDataInfo;
import com.lazy.common.enums.BusinessType;
import com.lazy.common.enums.SystemEnum;
import com.lazy.common.utils.ApiAssert;
import com.lazy.common.utils.StringUtils;
import com.lazy.common.utils.poi.ExcelUtil;
import com.lazy.system.domain.convert.ArticleConvert;
import com.lazy.system.domain.dto.AccountDto;
import com.lazy.system.domain.dto.AccountImgAndNickNameDto;
import com.lazy.system.domain.dto.ArticleAddDto;
import com.lazy.system.domain.entity.Article;
import com.lazy.system.domain.enums.TopicTypeEnum;
import com.lazy.system.domain.query.ArticleQuery;
import com.lazy.system.domain.vo.ArticleVo;
import com.lazy.system.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 文章Controller
 *
 * @author yang
 * @date 2021-01-21
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/applet/article")
@Api(tags = "文章相关服务", description = "ArticleController")
public class ArticleController extends BaseAppletController {

    private final IArticleService iArticleService;
    private final IAccountService accountService;
    private final IBrowseRecordService browseService;
    private final ILikeRecordService likeService;
    private final ICommentService commentService;
    private final ICommentReplyService commentReplyService;

    /**
     * 查询文章列表
     */
    @GetMapping("/list")
    @ApiOperation("列表")
    public TableDataInfo list(ArticleQuery dto) {
        startPage();
        LambdaQueryWrapper<Article> lqw = Wrappers.lambdaQuery();
        AccountDto accountDto = getAccountAndValidateTeacher();
        lqw.eq(dto.getSelf() == 1, Article::getUid, accountDto.getUid());
        lqw.eq(dto.getSelf() == 0, Article::getPub, 1);
        lqw.like(StringUtils.isNotBlank(dto.getTitle()), Article::getTitle, dto.getTitle());
        lqw.ge(dto.getPublishTimeStart() != null, Article::getPublishTime, dto.getPublishTimeStart());
        lqw.lt(dto.getPublishTimeEnd() != null, Article::getPublishTime, dto.getPublishTimeEnd());
        List<Article> list = iArticleService.list(lqw);
        List<ArticleVo> vos = list.stream().map(this::buildArticleVo).collect(Collectors.toList());
        return getDataTable(vos);
    }

    private ArticleVo buildArticleVo(Article article) {
        ArticleVo vo = ArticleConvert.INSTANCE.convert(article);
        AccountImgAndNickNameDto accountImgAndNickNameDto = accountService.getImgAndNameByUid(article.getUid());
        vo.setName(accountImgAndNickNameDto.getName());
        vo.setAvatar(accountImgAndNickNameDto.getAvatar());
        if (StringUtils.isNotEmpty(article.getUrls())) {
            String[] pic = article.getUrls().split(",");
            List<String> picList = Arrays.asList(pic);
            vo.setPic(picList);
        }
        int browseCount = browseService.browseCount(vo.getId(), TopicTypeEnum.ARTICLE.getCode());
        int likeCount = likeService.likeCount(vo.getId(), TopicTypeEnum.ARTICLE.getCode());
        int commentCount = commentService.commentCount(vo.getId(), TopicTypeEnum.ARTICLE.getCode());
        int commentReplyCount = commentReplyService.commentReplyCount(vo.getId(), TopicTypeEnum.ARTICLE.getCode());
        vo.setBrowseCount(browseCount);
        vo.setLikeCount(likeCount);
        vo.setCommentCount(commentCount + commentReplyCount);
        return vo;
    }

    /**
     * 导出文章列表
     */
    @Log(title = "文章", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation("导出")
    public AjaxResult export(ArticleQuery dto) {
        LambdaQueryWrapper<Article> lqw = new LambdaQueryWrapper<>();
        List<Article> list = iArticleService.list(lqw);
        ExcelUtil<Article> util = new ExcelUtil<>(Article.class);
        return util.exportExcel(list, "article");
    }

    /**
     * 获取觉醒者用户信息详细信息
     */
    @GetMapping(value = "/{id}")
    @ApiOperation("详情")
    public R<ArticleVo> getInfo(@PathVariable("id") Long id) {
        Article entity = iArticleService.getById(id);
        ArticleVo vo = buildArticleVo(entity);
        return new R<>(vo);
    }

    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "文章信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("添加")
    public R add(@RequestBody ArticleAddDto dto) {
        AccountDto accountDto = getAccountAndValidateTeacher();
        ApiAssert.isNull(SystemEnum.ID_NOT_NULL, dto.getId());
        Article entity = ArticleConvert.INSTANCE.convertAddDto(dto);
        entity.setUid(accountDto.getUid());
        entity.setPublishTime(new Date());
        return new R<>(iArticleService.save(entity));
    }

    /**
     * 修改觉醒者用户信息
     */
    @Log(title = "文章信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ApiOperation("修改")
    public R edit(@RequestBody ArticleAddDto dto) {
        AccountDto accountDto = getAccountAndValidateTeacher();
        ApiAssert.notNull(SystemEnum.ID_NULL, dto.getId());
        Article entity = ArticleConvert.INSTANCE.convertAddDto(dto);
        entity.setUid(accountDto.getUid());
        return new R<>(iArticleService.updateById(entity));
    }

    /**
     * 删除文章信息
     */
    @Log(title = "文章信息", businessType = BusinessType.DELETE)
    @GetMapping("/delete/{ids}")
    @ApiOperation("删除 逗号")
    public R remove(@PathVariable String ids) {
        ApiAssert.notNull(SystemEnum.ID_NULL, ids);
        String[] idArrays = ids.split(",");
        return new R<>(iArticleService.removeByIds(Arrays.asList(idArrays)));
    }
}
