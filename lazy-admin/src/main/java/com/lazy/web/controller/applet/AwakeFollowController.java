package com.lazy.web.controller.applet;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.annotation.Log;
import com.lazy.common.core.domain.R;
import com.lazy.common.core.page.TableDataInfo;
import com.lazy.common.enums.BusinessType;
import com.lazy.common.enums.SystemEnum;
import com.lazy.common.utils.ApiAssert;
import com.lazy.system.domain.convert.AwakeFollowConvert;
import com.lazy.system.domain.dto.AwakeFollowAddDto;
import com.lazy.system.domain.entity.AwakeFollow;
import com.lazy.system.domain.query.AwakeFollowQuery;
import com.lazy.system.domain.vo.AwakeFollowVo;
import com.lazy.system.service.IAccountService;
import com.lazy.system.service.IAwakeFollowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 我的关注的人Controller
 *
 * @author yang
 * @date 2021-01-23
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/applet/lazy/follow")
@Api(tags = "小程序我的关注的人相关服务", description = "AwakeFollowController")
public class AwakeFollowController extends BaseAppletController {

    private final IAwakeFollowService iAwakeFollowService;
    private final IAccountService accountService;

    /**
     * 查询我的关注的人列表
     */
    @GetMapping("/list")
    public TableDataInfo list(AwakeFollowQuery dto) {
        startPage();
        LambdaQueryWrapper<AwakeFollow> lqw = Wrappers.lambdaQuery();
        lqw.eq(dto.getUid() != null, AwakeFollow::getUid, dto.getUid());
        lqw.eq(dto.getFollowUid() != null, AwakeFollow::getFollowUid, dto.getFollowUid());
        lqw.eq(dto.getType() != null, AwakeFollow::getType, dto.getType());
        lqw.ge(dto.getFollowTimeStart() != null, AwakeFollow::getFollowTime, dto.getFollowTimeStart());
        lqw.lt(dto.getFollowTimeEnd() != null, AwakeFollow::getFollowTime, dto.getFollowTimeEnd());
        List<AwakeFollow> list = iAwakeFollowService.list(lqw);
        List<AwakeFollowVo> vos = list.stream().map(follow -> {
            AwakeFollowVo vo = AwakeFollowConvert.INSTANCE.convert(follow);
            vo.setFollowName(accountService.getNameByUid(vo.getFollowUid()));
            return vo;
        }).collect(Collectors.toList());

        return getDataTable(vos);
    }


    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "我的关注的人信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("添加")
    public R add(@RequestBody AwakeFollowAddDto dto) {
        ApiAssert.isNull(SystemEnum.ID_NOT_NULL, dto.getId());
        return new R<>(iAwakeFollowService.save(AwakeFollowConvert.INSTANCE.convertAddDto(dto)));
    }


    /**
     * 删除我的关注的人信息
     */
    @Log(title = "我的关注的人信息", businessType = BusinessType.DELETE)
    @GetMapping("/delete/{ids}")
    @ApiOperation("删除")
    public R remove(@PathVariable String ids) {
        ApiAssert.notNull(SystemEnum.ID_NULL, ids);
        String[] idArray = ids.split(",");
        return new R<>(iAwakeFollowService.removeByIds(Arrays.asList(idArray)));
    }
}
