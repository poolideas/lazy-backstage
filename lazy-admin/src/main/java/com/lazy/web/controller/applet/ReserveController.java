package com.lazy.web.controller.applet;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.annotation.Log;
import com.lazy.common.core.domain.R;
import com.lazy.common.core.page.TableDataInfo;
import com.lazy.common.enums.BusinessType;
import com.lazy.common.exception.CustomException;
import com.lazy.system.domain.convert.ReserveConvert;
import com.lazy.system.domain.dto.AccountDto;
import com.lazy.system.domain.dto.AccountImgAndNickNameDto;
import com.lazy.system.domain.dto.ReserveAddDto;
import com.lazy.system.domain.entity.Reserve;
import com.lazy.system.domain.enums.ReserveStatusEnum;
import com.lazy.system.domain.enums.UserTypeEnum;
import com.lazy.system.domain.query.ReserveQuery;
import com.lazy.system.domain.vo.ReserveVo;
import com.lazy.system.service.IAccountService;
import com.lazy.system.service.IReserveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 预约Controller
 *
 * @author yang
 * @date 2021-02-09
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/applet/reserve")
@Api(tags = "预约相关服务", description = "ReserveController")
public class ReserveController extends BaseAppletController {

    private final IReserveService iReserveService;
    private final IAccountService accountService;

    /**
     * 查询预约列表
     */
    @GetMapping("/list")
    @ApiOperation("列表")
    public TableDataInfo list(ReserveQuery dto) {

        AccountDto accountDto = getAccountDto();
        startPage();
        LambdaQueryWrapper<Reserve> lqw = Wrappers.lambdaQuery();
        lqw.eq(accountDto.getType().equals(UserTypeEnum.AWAKE.getCode()), Reserve::getUid, accountDto.getUid());
        lqw.eq(accountDto.getType().equals(UserTypeEnum.TEACHER.getCode()), Reserve::getTeacherUid, accountDto.getUid());
        lqw.eq(dto.getStatus() != null, Reserve::getStatus, dto.getStatus());
        List<Reserve> list = iReserveService.list(lqw);
        List<ReserveVo> vos = list.stream().map(this::buildReserveVo).collect(Collectors.toList());
        return getDataTable(vos);
    }

    private ReserveVo buildReserveVo(Reserve reserve) {
        ReserveVo vo = ReserveConvert.INSTANCE.convert(reserve);
        AccountImgAndNickNameDto account = accountService.getImgAndNameByUid(reserve.getUid());
        vo.setName(account.getName());
        vo.setAvatar(account.getAvatar());
        vo.setPhone(account.getPhone());
        AccountImgAndNickNameDto teacherAccount = accountService.getImgAndNameByUid(reserve.getTeacherUid());
        vo.setTeacherName(teacherAccount.getName());
        vo.setTeacherAvatar(teacherAccount.getAvatar());
        vo.setTeacherPhone(teacherAccount.getPhone());
        return vo;
    }


    /**
     * 获取觉醒者用户信息详细信息
     */
    @GetMapping(value = "/{id}")
    @ApiOperation("详情")
    public R<ReserveVo> getInfo(@PathVariable("id") Long id) {
        Reserve entity = iReserveService.getById(id);
        ReserveVo reserveVo = buildReserveVo(entity);
        return new R<>(reserveVo);
    }

    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "预约信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("申请")
    public R add(@RequestBody ReserveAddDto dto) {

        Reserve entity = ReserveConvert.INSTANCE.convertAddDto(dto);
        entity.setStatus(ReserveStatusEnum.APPLY.getCode());
        entity.setUid(getAccountAndValidateAwaker().getUid());
        return new R<>(iReserveService.save(entity));
    }

    @Log(title = "预约信息", businessType = BusinessType.INSERT)
    @PostMapping("/handler")
    @ApiOperation("教练接单")
    public R handler(@RequestBody ReserveAddDto dto) {

        Reserve reserve = iReserveService.getById(dto.getId());
        if (reserve == null || !reserve.getStatus().equals(ReserveStatusEnum.APPLY.getCode())) {
            throw new CustomException("预约状态异常，请联系管理员");
        }
        AccountDto accountDto = getAccountAndValidateTeacher();
        if (accountDto == null || !accountDto.getUid().equals(reserve.getTeacherUid())) {
            throw new CustomException("只能接受自己的预约单");
        }
        Reserve entity = new Reserve();
        entity.setId(dto.getId());
        entity.setStatus(ReserveStatusEnum.ACCEPT.getCode());
        entity.setServiceTime(dto.getServiceTime());
        return new R<>(iReserveService.updateById(entity));
    }

    @Log(title = "预约信息", businessType = BusinessType.INSERT)
    @PostMapping("/refuse")
    @ApiOperation("教练拒绝")
    public R refuse(@RequestBody ReserveAddDto dto) {

        Reserve reserve = iReserveService.getById(dto.getId());
        if (reserve == null || !reserve.getStatus().equals(ReserveStatusEnum.APPLY.getCode())) {
            throw new CustomException("预约状态异常，请联系管理员");
        }
        AccountDto accountDto = getAccountAndValidateTeacher();
        if (accountDto == null || !accountDto.getUid().equals(reserve.getTeacherUid())) {
            throw new CustomException("只能拒绝自己的预约单");
        }
        Reserve entity = new Reserve();
        entity.setId(dto.getId());
        entity.setStatus(ReserveStatusEnum.REFUSE.getCode());
        return new R<>(iReserveService.updateById(entity));
    }

    @Log(title = "预约信息", businessType = BusinessType.INSERT)
    @PostMapping("/cancel")
    @ApiOperation("觉醒者取消")
    public R cancel(@RequestBody ReserveAddDto dto) {

        Reserve reserve = iReserveService.getById(dto.getId());
        if (reserve == null || !reserve.getStatus().equals(ReserveStatusEnum.APPLY.getCode())) {
            throw new CustomException("预约状态异常，请联系管理员");
        }
        AccountDto accountDto = getAccountAndValidateAwaker();
        if (accountDto == null || !accountDto.getUid().equals(reserve.getUid())) {
            throw new CustomException("只能取消自己的预约单");
        }
        Reserve entity = new Reserve();
        entity.setId(dto.getId());
        entity.setStatus(ReserveStatusEnum.CANCEL.getCode());
        return new R<>(iReserveService.updateById(entity));
    }

}
