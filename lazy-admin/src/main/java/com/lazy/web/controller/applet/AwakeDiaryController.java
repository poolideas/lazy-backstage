package com.lazy.web.controller.applet;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lazy.common.annotation.Log;
import com.lazy.common.core.domain.R;
import com.lazy.common.core.page.TableDataInfo;
import com.lazy.common.enums.BusinessType;
import com.lazy.common.enums.SystemEnum;
import com.lazy.common.utils.ApiAssert;
import com.lazy.common.utils.DateUtils;
import com.lazy.common.utils.TagUtil;
import com.lazy.system.domain.convert.AwakeDiaryConvert;
import com.lazy.system.domain.dto.AccountDto;
import com.lazy.system.domain.dto.AccountImgAndNickNameDto;
import com.lazy.system.domain.dto.AwakeDiaryAddDto;
import com.lazy.system.domain.entity.AwakeDiary;
import com.lazy.system.domain.entity.GallupTag;
import com.lazy.system.domain.enums.TopicTypeEnum;
import com.lazy.system.domain.query.AwakeDiaryQuery;
import com.lazy.system.domain.vo.AwakeDiaryDoneVo;
import com.lazy.system.domain.vo.AwakeDiaryVo;
import com.lazy.system.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.lazy.common.utils.DateUtils.YYYY_MM_DD;

/**
 * 觉醒日记Controller
 *
 * @author yang
 * @date 2021-01-23
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/applet/diary")
@Api(tags = "小程序觉醒日记相关服务", description = "AwakeDiaryController")
public class AwakeDiaryController extends BaseAppletController {

    private final IAwakeDiaryService iAwakeDiaryService;
    private final IGallupTagService gallupTagService;
    private final IAccountService accountService;
    private final IBrowseRecordService browseService;
    private final ILikeRecordService likeService;
    private final ICommentService commentService;
    private final ICommentReplyService commentReplyService;

    /**
     * 查询觉醒日记列表
     */
    @GetMapping("/list")
    @ApiOperation("觉醒者根据时间查询列表")
    public TableDataInfo list(AwakeDiaryQuery dto) {
        startPage();
        LambdaQueryWrapper<AwakeDiary> lqw = Wrappers.lambdaQuery();
        lqw.eq(AwakeDiary::getUid, getAccountAndValidateAwaker().getUid());
        lqw.ge(dto.getBeginTime() != null, AwakeDiary::getCreateTime, dto.getBeginTime());
        if (dto.getEndTime() != null) {
            lqw.lt(AwakeDiary::getCreateTime, DateUtils.addDays(dto.getEndTime(), 1));
        }
        List<AwakeDiary> list = iAwakeDiaryService.list(lqw);
        return getDataTable(list.stream().map(this::buildAwakeDiaryVo).collect(Collectors.toList()));
    }

    @GetMapping("/teacher/list")
    @ApiOperation("觉醒者根据时间查询列表")
    public TableDataInfo teacherGetList(AwakeDiaryQuery dto) {
        startPage();
        LambdaQueryWrapper<AwakeDiary> lqw = Wrappers.lambdaQuery();
        lqw.eq(AwakeDiary::getPub, 1);
        lqw.ge(dto.getBeginTime() != null, AwakeDiary::getCreateTime, dto.getBeginTime());
        if (dto.getEndTime() != null) {
            lqw.lt(AwakeDiary::getCreateTime, DateUtils.addDays(dto.getEndTime(), 1));
        }
        List<AwakeDiary> list = iAwakeDiaryService.list(lqw);
        return getDataTable(list.stream().map(this::buildAwakeDiaryVo).collect(Collectors.toList()));
    }

    @GetMapping("/doneInfo")
    @ApiOperation("觉醒者根据时间查询日志完成情况")
    public R awakeDiaryDoneInfo(AwakeDiaryQuery dto) {
        LambdaQueryWrapper<AwakeDiary> lqw = Wrappers.lambdaQuery();
        lqw.ge(dto.getBeginTime() != null, AwakeDiary::getCreateTime, dto.getBeginTime());
        if (dto.getEndTime() != null) {
            lqw.lt(AwakeDiary::getCreateTime, DateUtils.addDays(dto.getEndTime(), 1));
        }
        lqw.eq(AwakeDiary::getUid, getAccountAndValidateAwaker().getUid());
        List<AwakeDiary> list = iAwakeDiaryService.list(lqw);
        List<String> existDates = list.stream().map(AwakeDiary::getCreateTime).map(d -> DateFormatUtils.format(d, YYYY_MM_DD)).collect(Collectors.toList());
        List<String> dates = DateUtils.findDates(dto.getBeginTime(), dto.getEndTime());
        List<AwakeDiaryDoneVo> vos = dates.stream().map(d -> AwakeDiaryDoneVo.builder()
                .date(DateUtils.parseDate(d)).done(existDates.contains(d) ? 1 : 0).build())
                .collect(Collectors.toList());
        return new R<>(vos);
    }


    /**
     * 获取觉醒者用户信息详细信息
     */
    @GetMapping(value = "detail/{id}")
    @ApiOperation("通过id获取详情")
    public R<AwakeDiaryVo> getInfo(@PathVariable("id") Long id) {
        AwakeDiary entity = iAwakeDiaryService.getById(id);
        return new R<>(buildAwakeDiaryVo(entity));
    }

    /**
     * 获取觉醒者用户信息详细信息
     */
    @GetMapping(value = "/tagCount")
    @ApiOperation("根据时间获取才干使用次数")
    public R<Map<String, Integer>> getTagCount(AwakeDiaryQuery dto) {
        LambdaQueryWrapper<AwakeDiary> lqw = Wrappers.lambdaQuery();
        lqw.ge(dto.getBeginTime() != null, AwakeDiary::getCreateTime, dto.getBeginTime());
        if (dto.getEndTime() != null) {
            lqw.lt(AwakeDiary::getCreateTime, DateUtils.addDays(dto.getEndTime(), 1));
        }
        lqw.eq(AwakeDiary::getUid, getAccountAndValidateAwaker().getUid());
        List<AwakeDiary> list = iAwakeDiaryService.list(lqw);
        List<String> tags = list.stream().map(AwakeDiary::getTags).collect(Collectors.toList());
        Map<Integer, Integer> map = TagUtil.getTagCount(tags);
        Map<String, Integer> result = new HashMap<>();
        map.forEach((code, count) -> {
            GallupTag tagByCode = gallupTagService.getTagByCode(code);
            result.put(tagByCode.getName(), count);
        });
        return new R<>(result);
    }


    /**
     * 获取觉醒者用户信息详细信息
     */
    @GetMapping(value = "/{date}")
    @ApiOperation("通过日期 获取详情 格式 yyyy-MM-dd")
    public R<AwakeDiaryVo> detail(@PathVariable("date") String date) {
        AccountDto accountDto = getAccountAndValidateAwaker();
        LambdaQueryWrapper<AwakeDiary> wrapper = Wrappers.lambdaQuery();
        wrapper.lt(AwakeDiary::getCreateTime, DateUtils.addDays(DateUtils.parseDate(date), 1))
                .ge(AwakeDiary::getCreateTime, DateUtils.parseDate(date))
                .eq(AwakeDiary::getUid, accountDto.getUid());
        AwakeDiary entity = iAwakeDiaryService.getOne(wrapper);
        return new R<>(buildAwakeDiaryVo(entity));
    }

    /**
     * 新增觉醒者用户信息
     */
    @Log(title = "觉醒日记信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("添加")
    public R add(@RequestBody AwakeDiaryAddDto dto) {
        AccountDto accountDto = getAccountAndValidateAwaker();
        ApiAssert.isNull(SystemEnum.ID_NOT_NULL, dto.getId());
        String[] tagArray = dto.getTags().split(",");
        String tags = TagUtil.getTagStrByList(Arrays.asList(tagArray));
        AwakeDiary entity = AwakeDiaryConvert.INSTANCE.convertAddDto(dto);
        entity.setUid(accountDto.getUid());
        entity.setTags(tags);
        return new R<>(iAwakeDiaryService.save(entity));
    }

    /**
     * 修改觉醒者用户信息
     */
    @Log(title = "觉醒日记信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ApiOperation("修改")
    public R edit(@RequestBody AwakeDiaryAddDto dto) {
        ApiAssert.notNull(SystemEnum.ID_NULL, dto.getId());
        String[] tagArray = dto.getTags().split(",");
        String tags = TagUtil.getTagStrByList(Arrays.asList(tagArray));
        AwakeDiary entity = AwakeDiaryConvert.INSTANCE.convertAddDto(dto);
        entity.setTags(tags);
        return new R<>(iAwakeDiaryService.updateById(entity));
    }

    /**
     * 删除觉醒日记信息
     */
    @Log(title = "觉醒日记信息", businessType = BusinessType.DELETE)
    @GetMapping("/delete/{ids}")
    @ApiOperation("删除")
    public R remove(@PathVariable String ids) {
        AccountDto accountDto = getAccountDto();
        String[] idArrays = ids.split(",");
        ApiAssert.notEmpty(SystemEnum.ID_NULL, idArrays);
        LambdaUpdateWrapper<AwakeDiary> wrapper = Wrappers.lambdaUpdate();
        wrapper.in(AwakeDiary::getId, (Object[]) idArrays);
        wrapper.eq(AwakeDiary::getUid, accountDto.getUid());
        return new R<>(iAwakeDiaryService.remove(wrapper));
    }


    /**
     * AwakeDiary 转换成 AwakeDiaryVo 标签code转换成name
     *
     * @param entity
     * @return
     */
    private AwakeDiaryVo buildAwakeDiaryVo(AwakeDiary entity) {
        if (entity == null) {
            return new AwakeDiaryVo();
        }
        AwakeDiaryVo vo = AwakeDiaryConvert.INSTANCE.convert(entity);
        List<GallupTag> list = gallupTagService.getTagListByTagStr(entity.getTags(), 34, 1);
        List<String> tagNames = list.stream().map(GallupTag::getName).collect(Collectors.toList());
        AccountImgAndNickNameDto dto = accountService.getImgAndNameByUid(entity.getUid());
        vo.setTagNames(tagNames);
        vo.setName(dto.getName());
        vo.setAvatar(dto.getAvatar());

        int browseCount = browseService.browseCount(vo.getId(), TopicTypeEnum.AWAKE_DIARY.getCode());
        int likeCount = likeService.likeCount(vo.getId(), TopicTypeEnum.AWAKE_DIARY.getCode());
        int commentCount = commentService.commentCount(vo.getId(), TopicTypeEnum.AWAKE_DIARY.getCode());
        int commentReplyCount = commentReplyService.commentReplyCount(vo.getId(), TopicTypeEnum.AWAKE_DIARY.getCode());
        vo.setBrowseCount(browseCount);
        vo.setLikeCount(likeCount);
        vo.setCommentCount(commentCount + commentReplyCount);
        return vo;
    }


}
