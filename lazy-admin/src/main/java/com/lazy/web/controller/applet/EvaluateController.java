//package com.lazy.web.controller.applet;
//
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.lazy.common.annotation.Log;
//import com.lazy.common.core.controller.BaseController;
//import com.lazy.common.core.domain.AjaxResult;
//import com.lazy.common.core.domain.R;
//import com.lazy.common.core.page.TableDataInfo;
//import com.lazy.common.enums.BusinessType;
//import com.lazy.common.enums.SystemEnum;
//import com.lazy.common.utils.ApiAssert;
//import com.lazy.common.utils.poi.ExcelUtil;
//import com.lazy.system.domain.convert.EvaluateConvert;
//import com.lazy.system.domain.dto.EvaluateAddDto;
//import com.lazy.system.domain.entity.Evaluate;
//import com.lazy.system.domain.query.EvaluateQuery;
//import com.lazy.system.domain.vo.EvaluateVo;
//import com.lazy.system.service.IEvaluateService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.Arrays;
//import java.util.List;
//
///**
// * 预约评价Controller
// *
// * @author yang
// * @date 2021-02-09
// */
//@RequiredArgsConstructor(onConstructor_ = @Autowired)
//@RestController
//@RequestMapping("/applet/evaluate")
//@Api(tags = "预约评价相关服务", description = "EvaluateController")
//public class EvaluateController extends BaseController {
//
//    private final IEvaluateService iEvaluateService;
//
//    /**
//     * 查询预约评价列表
//     */
//    @GetMapping("/list")
//    public TableDataInfo list(EvaluateQuery dto) {
//        startPage();
//        LambdaQueryWrapper<Evaluate> lqw = Wrappers.lambdaQuery();
//        lqw.eq(dto.getUid() != null, Evaluate::getUid, dto.getUid());
//        lqw.eq(dto.getTeacherUid() != null, Evaluate::getTeacherUid, dto.getTeacherUid());
//        lqw.eq(dto.getReserveId() != null, Evaluate::getReserveId, dto.getReserveId());
//        List<Evaluate> list = iEvaluateService.list(lqw);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出预约评价列表
//     */
//    @Log(title = "预约评价", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    @ApiOperation("导出")
//    public AjaxResult export(EvaluateQuery dto) {
//        LambdaQueryWrapper<Evaluate> lqw = new LambdaQueryWrapper<Evaluate>();
//        List<Evaluate> list = iEvaluateService.list(lqw);
//        ExcelUtil<Evaluate> util = new ExcelUtil<Evaluate>(Evaluate.class);
//        return util.exportExcel(list, "evaluate");
//    }
//
//    /**
//     * 获取觉醒者用户信息详细信息
//     */
//    @GetMapping(value = "/{id}")
//    @ApiOperation("详情")
//    public R<EvaluateVo> getInfo(@PathVariable("id") Long id) {
//        Evaluate entity = iEvaluateService.getById(id);
//        return new R<>(EvaluateConvert.INSTANCE.convert(entity));
//    }
//
//    /**
//     * 新增觉醒者用户信息
//     */
//    @Log(title = "预约评价信息", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    @ApiOperation("添加")
//    public R add(@RequestBody EvaluateAddDto dto) {
//        ApiAssert.isNull(SystemEnum.ID_NOT_NULL, dto.getId());
//        return new R<>(iEvaluateService.save(EvaluateConvert.INSTANCE.convertAddDto(dto)));
//    }
//
//    /**
//     * 修改觉醒者用户信息
//     */
//    @Log(title = "预约评价信息", businessType = BusinessType.UPDATE)
//    @PostMapping("/update")
//    @ApiOperation("修改")
//    public R edit(@RequestBody EvaluateAddDto dto) {
//        ApiAssert.notNull(SystemEnum.ID_NULL, dto.getId());
//        return new R<>(iEvaluateService.updateById(EvaluateConvert.INSTANCE.convertAddDto(dto)));
//    }
//
//    /**
//     * 删除预约评价信息
//     */
//    @Log(title = "预约评价信息", businessType = BusinessType.DELETE)
//    @GetMapping("/delete/{ids}")
//    @ApiOperation("删除")
//    public R remove(@PathVariable Long[] ids) {
//        ApiAssert.notNull(SystemEnum.ID_NULL, ids);
//        return new R<>(iEvaluateService.removeByIds(Arrays.asList(ids)));
//    }
//}
